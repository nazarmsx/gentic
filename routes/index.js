var express = require('express');
var router = express.Router();
var utils=require("../core/test.js");
var visual=require("../core/visual.js");
var fs=require('fs');
var statsUtils=require('../core/stats.js');
//console.log(getPoints(utils.f15,0,1));
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Візуалізація' });
});

router.get('/api/func/:func_name', function(req, res, next) {

  res.json(visual.getPoints(utils[req.params.func_name],parseFloat(req.query.start),parseFloat(req.query.end)));
});
router.get('/api/func3D/:func_name', function(req, res, next) {

  res.json(visual.getPoints3D(utils[req.params.func_name],parseFloat(req.query.start),parseFloat(req.query.end)));
});

var app = express();
var server = require('http').createServer(app);
var io = require('socket.io')(server);

app.use(express.static('./public'));
var allowCrossDomain = function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  if ('OPTIONS' == req.method) {
    res.sendStatus(200);
  }
  else {
    next();
  }

};
app.use(allowCrossDomain);


io.on('connect', function (socket) {
  console.log(socket);
})

server.listen(4001);

router.get('/stats', function(req, res, next) {
  res.render('stat', { title: 'Статистика прогонів '+Math.random() });
});
router.get('/api/all-stats', function(req, res, next) {
  fs.readFile('./core/data.json','utf-8',function(err,data){

  var stats=JSON.parse(data);

  res.json(stats);});
});

router.get('/api/stats/:func_name/:n', function(req, res, next) {
  var n=req.params.n?req.params.n:1;
  var func=req.params.func_name;
  fs.readFile('./core/data.json','utf-8',function(err,data){

    //console.log(err);
    var stats=JSON.parse(data);

    var result=stats.map(e=>{
      var t=e[func]['n'+n];
      return t;
    })
    res.json(result);

  });
});
router.get('/api/config', function(req, res, next) {
  res.json(statsUtils.configurations);
});

router.get('/api/function-peaks/:func/:dim', function(req, res, next) {
  var info=utils[req.params.func+'_config'];
  res.json({peaks:info.peakCount(req.params.dim)});
});

module.exports = router;
