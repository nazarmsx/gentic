'use strict';

var app = angular.module('myApp', []);

app.controller('indexCtrl', ['$scope', '$http', function ($scope, $http) {

    $http.get('/api/config').success(function (data) {
        $scope.configs = data;
    });
    $scope.curFunc = 'f15';
    $scope.curDim = 1;

    $('#sel1').on('change', function () {
        $scope.curFunc = this.value;
        $scope.populateStats();
        $('.test-func').text($('#sel1 option:selected').text());
        //console.log($('#sel1 option:selected'));
    });
    $scope.$watch('curDim', function (newValue, oldValue) {
        if (newValue != oldValue) {
            $scope.populateStats();
        }
        ;
    });
    $scope.getAllStats = function () {
        $http.get('/api/all-stats').success(function (data) {
            $scope.allData = data;
            $scope.generalStats();
        });
    };
    $scope.generalStats = function () {
        var func = 'f15';
        var n = 1;
        var funcs = Object.keys($scope.allData[0]);
        var dims = Object.keys($scope.allData[0]['f15']);
        console.log(funcs);
        console.log(dims);
        $scope.configsStats = $scope.configs.map(function (e) {
            return [];
        });
        for (var z = 0; z < funcs.length; ++z) {
            for (var k = 0; k < dims.length; ++k) {

                $scope.configs.map(function (stats, index) {
                    // console.log('Stats for :' + funcs[z] + ' ' + dims[k]);

                    var result = $scope.allData.map(e=> {
                        var t = e[funcs[z]][dims[k]][index];
                        return t;
                    });

                    var res = {
                        all_peaks_found: 0,
                        avg_peaks_found: 0,
                        NFE_a: 0,
                        NP_a: 0,
                        PR_a: 0,
                        PA_a: 0,
                        DA_a: 0,
                        NFE_b: result[0].NFE,
                        NP_b: result[0].number_of_peaks,
                        PR_b: result[0].effective_number,
                        PA_b: result[0].peak_accuracy,
                        DA_b: result[0].distance_accuracy,
                    };
                    var successCount = 0;
                    //console.log($scope.functionMeta[funcs[z]])
                    var functionConfig = $scope.functionMeta[funcs[z]];
                    for (var i = 0; i < result.length; ++i) {

                        //if (functionConfig.peaks && result[i].real_peaks >= functionConfig.peaks.length)
                        //    successCount++;
                        res.NFE_a += result[i].NFE;
                        res.NP_a += result[i].real_peaks;
                        res.PR_a += result[i].effective_number;
                        res.PA_a += result[i].peak_accuracy;
                        res.DA_a += result[i].distance_accuracy;

                        if (result[i].NFE < res.NFE_b) {
                            res.NFE_b = result[i].NFE;
                        }
                        if (result[i].number_of_peaks > res.NP_b) {
                            res.NP_b = result[i].number_of_peaks;
                        }
                        if (result[i].effective_number > res.PR_b) {
                            res.PR_b = result[i].effective_number;
                        }
                        if (result[i].peak_accuracy > res.PA_b) {
                            res.PA_b = result[i].peak_accuracy;
                        }
                        if (result[i].distance_accuracy > res.DA_b) {
                            res.DA_b = result[i].distance_accuracy;
                        }

                    }
                    //console.log(result);

                    res.all_peaks_found = (100 / result.length) * successCount;
                    res.NFE_a /= result.length;
                    res.NP_a /= result.length;
                    res.PR_a /= result.length;
                    res.PA_a /= result.length;
                    res.DA_a /= result.length;
                    $scope.configsStats[index].push(res);

                });
            }

        }

        //console.log($scope.configsStats);

        $scope.local = $scope.configsStats.map(function (elem) {

            return elem.reduce((pr, el)=> {

                for (var prop in el) {
                    if (el[prop])
                        pr[prop] += el[prop];
                }
                return pr;
            }, JSON.parse(JSON.stringify(elem[0])))


        });
        $scope.propsCompare = {
            all_peaks_found: ">",
            avg_peaks_found: ">",
            PR_b: ">",
            PR_a: ">",
            NP_a: ">",
            NP_b: ">",
            PA_a: ">",
            PA_b: ">",
            NFE_a: "<",
            NFE_b: "<",
            DA_a: "<",
            DA_b: "<"


        };
        $scope.max = $scope.configsStats.map(function (elem) {
            return elem.reduce((pr, el)=> {



                for (var prop in el) {
                    // console.log(prop )

                    //console.log(el[prop] )
                    // console.log(pr[prop] )


                    if(el[prop]==null)
                    continue;
                    if ($scope.propsCompare[prop] == '>') {
                        console.log(pr.NP_a);

                        if (pr[prop] < el[prop]) {
                            pr[prop] = el[prop];
                        }
                    } else {
                        if (pr[prop] > el[prop]) {
                            pr[prop] = el[prop];

                        }
                        console.log(pr.NP_a);

                    }


                }
                return pr;
            }, JSON.parse(JSON.stringify(elem[0])))


        });

        console.log($scope.max[0]);

        //-------------------------
        //for(var i=0;i<$scope.allData.length;++i){
        //for(var func in $scope.allData[i]){
        //    for(var dim in $scope.allData[i][func]){
        //        console.log(func);
        //        console.log(dim);
        //
        //        //var result=$scope.allData.map(e=>{
        //        //
        //        //    console.log(e['f15']);
        //        //    console.log(dim)
        //        //    return e[func]['n'+dim];
        //        //});
        //        ////console.log(result);
        //    }
        //}
        //}
        //
        //$scope.local=$scope.configs.map(function(stats,index){
        //    var configStats=[];
        //
        //    for(var i=0;i<$scope.allData.length;++i){
        //        for(var func in $scope.allData[i]){
        //            for(var dim in $scope.allData[i][func]){
        //                //console.log($scope.allData[i][func][dim][index])
        //                configStats.push($scope.allData[i][func][dim][index])
        //            }
        //        }
        //    }
        //    for(var func in $scope.allData[i]){
        //        for(var dim in $scope.allData[i][func]){
        //
        //            var result=$scope.allData.map(e=>{
        //                return e[func]['n'+dim];
        //            });
        //        }
        //    }
        //
        //    var res={
        //        all_peaks_found:0,
        //        avg_peaks_found:0,
        //        NFE_a:0,
        //        NP_a:0,
        //        PR_a:0,
        //        PA_a:0,
        //        DA_a:0,
        //        NFE_b:configStats[0].NFE,
        //        NP_b:configStats[0].number_of_peaks,
        //        PR_b:configStats[0].effective_number,
        //        PA_b:configStats[0].peak_accuracy,
        //        DA_b:configStats[0].distance_accuracy,
        //    };
        //    //console.log(res)
        //    var successCount=0;
        //    for(var i=0;i<configStats.length;++i)
        //    {
        //        //if(configStats.real_peaks>=$scope.functionConfig.peaks)
        //        //    successCount++;
        //
        //        res.NFE_a+=configStats[i].NFE;
        //        res.NP_a+=configStats[i].real_peaks;
        //        res.PR_a+=configStats[i].effective_number;
        //       // res.PA_a+=configStats[i].peak_accuracy;
        //       // res.DA_a+=configStats[i].distance_accuracy;
        //
        //        if(configStats[i].NFE<res.NFE_b){
        //            res.NFE_b=configStats[i].NFE;
        //        }
        //        if(configStats[i].number_of_peaks>res.NP_b){
        //            res.NP_b=configStats[i].number_of_peaks;
        //        }
        //        if(configStats[i].effective_number>res.PR_b){
        //            res.PR_b=configStats[i].effective_number;
        //        }
        //        if(configStats[i].peak_accuracy>res.PA_b){
        //            res.PA_b=configStats[i].peak_accuracy;
        //        }
        //        if(configStats[i].distance_accuracy>res.DA_b){
        //            res.DA_b=configStats[i].distance_accuracy;
        //        }
        //
        //    }
        //    res.all_peaks_found=(100/configStats.length)*successCount;
        //    res.NFE_a/=configStats.length;
        //    res.NP_a/=configStats.length;
        //    res.PR_a/=configStats.length;
        //    res.PA_a/=configStats.length;
        //    res.DA_a/=configStats.length;
        //    return res;
        //});

    };
    $scope.getAllStats();

    $scope.populateStats = function () {
        $http.get('/api/function-peaks/' + $scope.curFunc + '/' + $scope.curDim).success(function (data) {
            $scope.functionConfig = data;
        });
        $http.get('/api/stats/' + $scope.curFunc + '/' + $scope.curDim).success(function (data) {
            $scope.stats = data;


            $scope.stats2 = data.reduce(function (pr, cur) {
                pr = pr.concat(cur);
                return pr;
            }, []);

            $scope.allStats = $scope.configs.map(function (stats, index) {

                var res = {
                    all_peaks_found: 0,
                    avg_peaks_found: 0,
                    NFE_a: 0,
                    NP_a: 0,
                    PR_a: 0,
                    PA_a: 0,
                    DA_a: 0,
                    NFE_b: $scope.stats[0][index].NFE,
                    NP_b: $scope.stats[0][index].number_of_peaks,
                    PR_b: $scope.stats[0][index].effective_number,
                    PA_b: $scope.stats[0][index].peak_accuracy,
                    DA_b: $scope.stats[0][index].distance_accuracy,
                };
                var successCount = 0;
                for (var i = 0; i < $scope.stats.length; ++i) {

                    if ($scope.functionConfig.peaks && $scope.stats[i][index].real_peaks >= $scope.functionConfig.peaks.length)
                        successCount++;
                    res.NFE_a += $scope.stats[i][index].NFE;
                    res.NP_a += $scope.stats[i][index].real_peaks;
                    res.PR_a += $scope.stats[i][index].effective_number;
                    res.PA_a += $scope.stats[i][index].peak_accuracy;
                    res.DA_a += $scope.stats[i][index].distance_accuracy;

                    if ($scope.stats[i][index].NFE < res.NFE_b) {
                        res.NFE_b = $scope.stats[i][index].NFE;
                    }
                    if ($scope.stats[i][index].number_of_peaks > res.NP_b) {
                        res.NP_b = $scope.stats[i][index].number_of_peaks;
                    }
                    if ($scope.stats[i][index].effective_number > res.PR_b) {
                        res.PR_b = $scope.stats[i][index].effective_number;
                    }
                    if ($scope.stats[i][index].peak_accuracy > res.PA_b) {
                        res.PA_b = $scope.stats[i][index].peak_accuracy;
                    }
                    if ($scope.stats[i][index].distance_accuracy > res.DA_b) {
                        res.DA_b = $scope.stats[i][index].distance_accuracy;
                    }

                }
                res.all_peaks_found = (100 / $scope.stats.length) * successCount;
                res.NFE_a /= $scope.stats.length;
                res.NP_a /= $scope.stats.length;
                res.PR_a /= $scope.stats.length;
                res.PA_a /= $scope.stats.length;
                res.DA_a /= $scope.stats.length;
                return res;
            });
            //console.log($scope.allStats);

        })
    };

    $scope.populateStats();

    $scope.generateDimensions = function () {
        $scope.dimensions = [];
        for (var i = 1; i <= 5; ++i) {
            $scope.dimensions.push(i);
        }
    }
    $scope.generateDimensions();
    $scope.functionMeta = {
        f15: {min: 0, max: 1, peakCount: n=>Math.pow(5, n), peaks: [0.1, 0.3, 0.5, 0.7, 0.9]},
        f16: {min: 0, max: 1, peakCount: n=>Math.pow(5, n), peaks: [0.1, 0.3, 0.5, 0.7, 0.9]},
        f18: {min: 0, max: 1, peakCount: n=>Math.pow(5, n), peaks: [0.08, 0.247, 0.451, 0.681, 0.934]},
        f19: {min: 0, max: 1, peakCount: n=>Math.pow(5, n), peaks: [0.08, 0.247, 0.451, 0.681, 0.934]},
        f20: {min: -5.12, max: 5.12, peakCount: n=>Math.pow(10, n)},
        f22: {min: -100, max: 100},
        f46: {min: -3, max: 3, min_y: -2, max_y: 2},
        f24: {min: -10, max: 10, peakCount: n=>3 * Math.pow(3, n)},
    }

}]);
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});