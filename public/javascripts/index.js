

'use strict';

var app = angular.module('myApp',[]);

app.controller('indexCtrl', ['$scope','$http', function($scope,$http) {

    $scope.generateDimensions=function(){
        $scope.dimensions=[];
        for(var i=1;i<=2;++i){
            $scope.dimensions.push(i);
        }
    };
    $scope.generateDimensions();
    $scope.curDim=1;


    $scope.$watch('curDim',function(newValue,oldValue){
        if(newValue!=oldValue){
            console.log(newValue)
            $scope.populateDefaultGraf();

            //$scope.populateStats();
        };
    });
    $scope.$watch('curFunc',function(newValue,oldValue){
        if(newValue!=oldValue){
            console.log(newValue)
            $scope.populateDefaultGraf();
            //$scope.populateStats();
        };
    });
    $scope.populateDefaultGraf=function(){

        var curFunc=$scope.curFunc;
        var data=mapper[curFunc];

        if($scope.curDim==1){
            $.get( "/api/func/"+curFunc+"?start="+data.min+"&end="+data.max, function( data ) {
                var trace1 = {
                    x: [],
                    y: [],
                    type: 'scatter',
                    name:"Графік функції",

                };
                trace1.x=data.x;
                trace1.y=data.y;
                window.trace1=trace1;
                data = [trace1];

                Plotly.newPlot('myDiv', data);
            });
        }
         if($scope.curDim==2)
        {                console.log(data);

            $.get( "/api/func3D/"+curFunc+"?start="+data.min+"&end="+data.max, function( data ) {
                window.dtrace={
                    opacity:0.8,
                    type: 'mesh3d',
                    x: data.x,
                    y: data.y,
                    z: data.z
                };
                var data=[
                    window.dtrace
                ];
                Plotly.newPlot('3d', data);
            });
        }



            //$.get( "/api/func3D/"+curFunc+"?start="+data.min+"&end="+data.max, function( data ) {
            //    var data=[
            //        {
            //            opacity:0.8,
            //            type: 'mesh3d',
            //            x: data.x,
            //            y: data.y,
            //            z: data.z,
            //        }
            //    ];
            //    Plotly.newPlot('3d', data);
            //});
    };



    var generations=[];
    var curFunc='f15';
    $scope.curFunc=curFunc;


    $.get("/api/func/"+curFunc+"?start=0&end=1", function( data ) {
        var trace1 = {
            x: [1,1.5, 2, 3, 4],
            y: [10,4, 15, 13, 17],
            type: 'scatter',
            name:"Графік функції",

        };
        trace1.x=data.x;
        trace1.y=data.y;
        window.trace1=trace1;
        data = [trace1];
        Plotly.newPlot('myDiv', data);
    });




    $('#f15').click(()=>{
        socket.emit('solve',{func:$scope.curFunc,dim:$scope.curDim});
    });
    var socket = io().connect('http://127.0.0.1:4001');
    socket.on('connection', function(data) {

    });
    //
    //$('select').on('change', function() {
    //    curFunc=this.value;
    //    console.log(this.text);
    //    var data=mapper[curFunc];
    //    $.get( "/api/func/"+curFunc+"?start="+data.min+"&end="+data.max, function( data ) {
    //        var trace1 = {
    //            x: [1,1.5, 2, 3, 4],
    //            y: [10,4, 15, 13, 17],
    //            type: 'scatter',
    //            name:"Графік функції",
    //
    //        };
    //        trace1.x=data.x;
    //        trace1.y=data.y;
    //        window.trace1=trace1;
    //        data = [trace1];
    //        Plotly.newPlot('myDiv', data);
    //    });
    //    //$.get( "/api/func3D/"+curFunc+"?start="+data.min+"&end="+data.max, function( data ) {
    //    //    var data=[
    //    //        {
    //    //            opacity:0.8,
    //    //            type: 'mesh3d',
    //    //            x: data.x,
    //    //            y: data.y,
    //    //            z: data.z,
    //    //        }
    //    //    ];
    //    //    Plotly.newPlot('3d', data);
    //    //});
    //})

    var mapper={

        f15:{min:0,max:1,name:""},
        f16:{min:0,max:1,name:""},
        f18:{min:0,max:1,name:""},
        f19:{min:0,max:1,name:""},
        f20:{min:-5.12,max:5.12,name:""},
        f22:{min:-100,max:100,name:""},
        f24:{min:-10,max:10,name:""}
    };

    socket.on('connect', function () {
    });
    var $elem=document.getElementById("myDiv");
    var $div3d=document.getElementById("3d");


    socket.on('generation', function (data) {
        console.log('generation');
        data=JSON.parse(data);
        if(data.final){
            var counter=0;
            //var divider=Math.floor(generations.length*0.2);
            $("#total").text(generations.length);

            //generations=generations.filter((e,index)=>index%divider==0);

            async.mapLimit(generations,1,function(generation,cb){
                var current=generation;

                var trace2 = {
                    mode: 'markers',
                    name:"Особини",
                    marker: {
                        color: 'rgb(234, 0, 0)',
                        size: 4
                    }
                };

                var filtered={x:[],y:[],z:[]};

                for(var i=0;i<current.x.length;++i){
                    if(current.x[i]>=mapper[$scope.curFunc].min && current.x[i]<=mapper[$scope.curFunc].max){

                        if(current.z[i] && current.z[i]>=mapper[$scope.curFunc].min && current.z[i]<=mapper[$scope.curFunc].max){
                            filtered.z.push(current.z[i])
                            filtered.x.push(current.x[i]);
                            filtered.y.push(current.y[i]);
                        }else{
                            filtered.x.push(current.x[i]);
                            filtered.y.push(current.y[i]);
                        }

                    }
                }
                trace2.x=filtered.x;
                trace2.y=filtered.y;

                if(filtered.z.length>0){

                    var data=[
                        {
                            opacity:1.0,
                            mode:'markers',
                            x: filtered.x,
                            y: filtered.z,
                            z: filtered.y,
                            marker: {
                                size: 4,
                                line: {
                                    color:'rgb(234, 0, 0)',
                                    width: 0.5},
                                opacity: 0.8},
                            type: 'scatter3d'

                        }
                    ];

                   // console.log(data);
                    if(counter>0)
                    Plotly.deleteTraces($div3d, 1);


                    Plotly.addTraces($div3d, data);

                }
                else
                {
                    var config = [trace1,trace2];

                    Plotly.newPlot($elem, config);

                }
                setTimeout(function(){
                    cb(null,"ok");
                    $("#cur").text(++counter);

                },100)
            },function(err,results){
                generations=[];
                console.log(generations.length)
            });


        }
        else{
            generations.push(data);
        }




    });
    if($scope.curDim==2){
        $scope.populateDefaultGraf();
    }
}]);
app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');
});