var globalStats=[
    {
        "f15": {
            "n1": [
                {
                    "NFE": 9200,
                    "number_of_peaks": 5,
                    "real_peaks": 3,
                    "effective_number": 1,
                    "peak_accuracy": 0.4710118858990012,
                    "distance_accuracy": 4.414455891256061
                },
                {
                    "NFE": 7400,
                    "number_of_peaks": 14,
                    "real_peaks": 3,
                    "effective_number": 2.8,
                    "peak_accuracy": 3.000023114800171,
                    "distance_accuracy": 5.27279891100318
                },
                {
                    "NFE": 11400,
                    "number_of_peaks": 14,
                    "real_peaks": 3,
                    "effective_number": 2.8,
                    "peak_accuracy": 2.3747607292245534,
                    "distance_accuracy": 4.792125878001569
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 9,
                    "real_peaks": 4,
                    "effective_number": 1.8,
                    "peak_accuracy": 4.98977687856677,
                    "distance_accuracy": 4.250708922077435
                }
            ],
            "n2": [
                {
                    "NFE": 9600,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                },
                {
                    "NFE": 10200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.72
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.6
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                }
            ],
            "n3": [
                {
                    "NFE": 10000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.152
                },
                {
                    "NFE": 23800,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.152
                },
                {
                    "NFE": 11000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                }
            ],
            "n4": [
                {
                    "NFE": 11600,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.0176
                },
                {
                    "NFE": 12600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 20400,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.0336
                },
                {
                    "NFE": 12750,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                }
            ],
            "n5": [
                {
                    "NFE": 18200,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.00192
                },
                {
                    "NFE": 11600,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.00608
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 25,
                    "real_peaks": 25,
                    "effective_number": 0.008
                },
                {
                    "NFE": 13500,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00224
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 9200,
                    "number_of_peaks": 15,
                    "real_peaks": 5,
                    "effective_number": 3,
                    "peak_accuracy": 3.5155760016784936,
                    "distance_accuracy": 1.3194005609497743
                },
                {
                    "NFE": 20000,
                    "number_of_peaks": 13,
                    "real_peaks": 3,
                    "effective_number": 2.6,
                    "peak_accuracy": 3.354400262558163,
                    "distance_accuracy": 1.5971145042273793
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 19,
                    "real_peaks": 2,
                    "effective_number": 3.8,
                    "peak_accuracy": 2.5875159592883583,
                    "distance_accuracy": 2.5095731980883187
                },
                {
                    "NFE": 12250,
                    "number_of_peaks": 7,
                    "real_peaks": 2,
                    "effective_number": 1.4,
                    "peak_accuracy": 2.6465297395279688,
                    "distance_accuracy": 2.147953494607288
                }
            ],
            "n2": [
                {
                    "NFE": 10000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.6
                },
                {
                    "NFE": 21600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.64
                },
                {
                    "NFE": 17000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.08
                }
            ],
            "n3": [
                {
                    "NFE": 12600,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.056
                },
                {
                    "NFE": 15200,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.152
                },
                {
                    "NFE": 19200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.104
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.08
                }
            ],
            "n4": [
                {
                    "NFE": 11800,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.0208
                },
                {
                    "NFE": 18200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 21800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.0272
                },
                {
                    "NFE": 18000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0096
                }
            ],
            "n5": [
                {
                    "NFE": 13800,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00224
                },
                {
                    "NFE": 20400,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.00416
                },
                {
                    "NFE": 27000,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00288
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 7400,
                    "number_of_peaks": 11,
                    "real_peaks": 2,
                    "effective_number": 2.2,
                    "peak_accuracy": 1.4481914938717875,
                    "distance_accuracy": 4.369987249964243
                },
                {
                    "NFE": 12200,
                    "number_of_peaks": 14,
                    "real_peaks": 3,
                    "effective_number": 2.8,
                    "peak_accuracy": 2.069938585335611,
                    "distance_accuracy": 4.710161311207203
                },
                {
                    "NFE": 20200,
                    "number_of_peaks": 14,
                    "real_peaks": 3,
                    "effective_number": 2.8,
                    "peak_accuracy": 0.485465668696085,
                    "distance_accuracy": 3.7935407434739825
                },
                {
                    "NFE": 9500,
                    "number_of_peaks": 5,
                    "real_peaks": 2,
                    "effective_number": 1,
                    "peak_accuracy": 2.308841518932515,
                    "distance_accuracy": 3.8851646612171975
                }
            ],
            "n2": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.48
                },
                {
                    "NFE": 10200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.8
                },
                {
                    "NFE": 22800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.84
                },
                {
                    "NFE": 10000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.48
                }
            ],
            "n3": [
                {
                    "NFE": 9200,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.12
                },
                {
                    "NFE": 11800,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 16800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.168
                },
                {
                    "NFE": 12250,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                }
            ],
            "n4": [
                {
                    "NFE": 13600,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                },
                {
                    "NFE": 25400,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.0336
                },
                {
                    "NFE": 14500,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                }
            ],
            "n5": [
                {
                    "NFE": 12000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 15200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 31000,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 11500,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.00384
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 10000,
                    "number_of_peaks": 8,
                    "real_peaks": 1,
                    "effective_number": 1.6,
                    "peak_accuracy": 2.0391787203878575,
                    "distance_accuracy": 2.275021829580487
                },
                {
                    "NFE": 8600,
                    "number_of_peaks": 16,
                    "real_peaks": 3,
                    "effective_number": 3.2,
                    "peak_accuracy": 2.4528480131018013,
                    "distance_accuracy": 1.7879866345883237
                },
                {
                    "NFE": 15800,
                    "number_of_peaks": 17,
                    "real_peaks": 2,
                    "effective_number": 3.4,
                    "peak_accuracy": 3.329144250196792,
                    "distance_accuracy": 1.6405353705440493
                },
                {
                    "NFE": 9500,
                    "number_of_peaks": 10,
                    "real_peaks": 3,
                    "effective_number": 2,
                    "peak_accuracy": 3.1435399451790524,
                    "distance_accuracy": 1.1948684596730095
                }
            ],
            "n2": [
                {
                    "NFE": 11600,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                },
                {
                    "NFE": 12400,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.64
                },
                {
                    "NFE": 19800,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.56
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                }
            ],
            "n3": [
                {
                    "NFE": 12800,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                },
                {
                    "NFE": 17800,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 28800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                }
            ],
            "n4": [
                {
                    "NFE": 10000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.0192
                },
                {
                    "NFE": 15400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                },
                {
                    "NFE": 18800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.0336
                },
                {
                    "NFE": 11750,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                }
            ],
            "n5": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00256
                },
                {
                    "NFE": 16400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.00576
                },
                {
                    "NFE": 30000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.00512
                },
                {
                    "NFE": 18500,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00256
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 10800,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 1.1
                },
                {
                    "NFE": 13800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 2.1
                },
                {
                    "NFE": 39750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 11200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.08
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.12
                },
                {
                    "NFE": 15400,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.13
                },
                {
                    "NFE": 9000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.02
                }
            ],
            "n3": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.008
                },
                {
                    "NFE": 54000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.007
                },
                {
                    "NFE": 22000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.016
                },
                {
                    "NFE": 95000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 35200,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0006
                },
                {
                    "NFE": 80000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0006
                },
                {
                    "NFE": 33400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0002
                },
                {
                    "NFE": 73750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 27600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00002
                },
                {
                    "NFE": 33400,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.00004
                },
                {
                    "NFE": 61000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 28500,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.00003
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 10800,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 1.2222222222222223
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 1.5555555555555556
                },
                {
                    "NFE": 16800,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 2.111111111111111
                },
                {
                    "NFE": 10250,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.3333333333333333
                }
            ],
            "n2": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.4074074074074074
                },
                {
                    "NFE": 15600,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.6666666666666666
                },
                {
                    "NFE": 58800,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.6666666666666666
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.37037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 22400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.14814814814814814
                },
                {
                    "NFE": 48600,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.13580246913580246
                },
                {
                    "NFE": 18250,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.06172839506172839
                }
            ],
            "n4": [
                {
                    "NFE": 13800,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0411522633744856
                },
                {
                    "NFE": 22000,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.053497942386831275
                },
                {
                    "NFE": 38600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.06584362139917696
                },
                {
                    "NFE": 20250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.03292181069958848
                }
            ],
            "n5": [
                {
                    "NFE": 10800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0205761316872428
                },
                {
                    "NFE": 21800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.023319615912208505
                },
                {
                    "NFE": 35400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.02606310013717421
                },
                {
                    "NFE": 21250,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.015089163237311385
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 10800,
                    "number_of_peaks": 6,
                    "real_peaks": 2,
                    "effective_number": 1.2,
                    "peak_accuracy": 0.0001000646652806747,
                    "distance_accuracy": 3.9769392013019518
                },
                {
                    "NFE": 7800,
                    "number_of_peaks": 13,
                    "real_peaks": 4,
                    "effective_number": 2.6,
                    "peak_accuracy": 4.435733825576725,
                    "distance_accuracy": 3.739353005208811
                },
                {
                    "NFE": 14600,
                    "number_of_peaks": 11,
                    "real_peaks": 2,
                    "effective_number": 2.2,
                    "peak_accuracy": 4.996918514758478,
                    "distance_accuracy": 3.431973025562181
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 11,
                    "real_peaks": 5,
                    "effective_number": 2.2,
                    "peak_accuracy": 0.020377918793763383,
                    "distance_accuracy": 3.9589243731534
                }
            ],
            "n2": [
                {
                    "NFE": 8000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.64
                },
                {
                    "NFE": 9600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.64
                },
                {
                    "NFE": 19400,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.92
                },
                {
                    "NFE": 10000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                }
            ],
            "n3": [
                {
                    "NFE": 12600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.064
                },
                {
                    "NFE": 12600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.12
                },
                {
                    "NFE": 19600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.064
                }
            ],
            "n4": [
                {
                    "NFE": 14200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                },
                {
                    "NFE": 15200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 21000,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.0304
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                }
            ],
            "n5": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 21800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.00672
                },
                {
                    "NFE": 15500,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 12000,
                    "number_of_peaks": 5,
                    "real_peaks": 0,
                    "effective_number": 1,
                    "peak_accuracy": 2.966113913774642,
                    "distance_accuracy": 2.877137017466025
                },
                {
                    "NFE": 8200,
                    "number_of_peaks": 20,
                    "real_peaks": 5,
                    "effective_number": 4,
                    "peak_accuracy": 2.993355816655444,
                    "distance_accuracy": 1.6701411297803432
                },
                {
                    "NFE": 19800,
                    "number_of_peaks": 16,
                    "real_peaks": 2,
                    "effective_number": 3.2,
                    "peak_accuracy": 3.6796055733433146,
                    "distance_accuracy": 1.4172996796149728
                },
                {
                    "NFE": 10500,
                    "number_of_peaks": 7,
                    "real_peaks": 3,
                    "effective_number": 1.4,
                    "peak_accuracy": 2.5825774958132266,
                    "distance_accuracy": 2.1734987288698746
                }
            ],
            "n2": [
                {
                    "NFE": 9400,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                },
                {
                    "NFE": 16600,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                },
                {
                    "NFE": 26800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.6
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                }
            ],
            "n3": [
                {
                    "NFE": 13200,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.088
                },
                {
                    "NFE": 8800,
                    "number_of_peaks": 24,
                    "real_peaks": 24,
                    "effective_number": 0.192
                },
                {
                    "NFE": 22600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.08
                }
            ],
            "n4": [
                {
                    "NFE": 14600,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                },
                {
                    "NFE": 14400,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 22600,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.0336
                },
                {
                    "NFE": 14750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                }
            ],
            "n5": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                },
                {
                    "NFE": 17800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 27800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 16750,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00288
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 9000,
                    "number_of_peaks": 7,
                    "real_peaks": 1,
                    "effective_number": 1.4,
                    "peak_accuracy": 2.0001736910017276,
                    "distance_accuracy": 4.778398562250631
                },
                {
                    "NFE": 10800,
                    "number_of_peaks": 8,
                    "real_peaks": 2,
                    "effective_number": 1.6,
                    "peak_accuracy": 2.027395783352366,
                    "distance_accuracy": 4.563967378919134
                },
                {
                    "NFE": 9800,
                    "number_of_peaks": 20,
                    "real_peaks": 2,
                    "effective_number": 4,
                    "peak_accuracy": 1.1548730693401157,
                    "distance_accuracy": 3.6839586952083243
                },
                {
                    "NFE": 9500,
                    "number_of_peaks": 9,
                    "real_peaks": 2,
                    "effective_number": 1.8,
                    "peak_accuracy": 2.0002004371800424,
                    "distance_accuracy": 4.780768157584217
                }
            ],
            "n2": [
                {
                    "NFE": 8800,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.56
                },
                {
                    "NFE": 13800,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.48
                },
                {
                    "NFE": 21400,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.64
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                }
            ],
            "n3": [
                {
                    "NFE": 11200,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.08
                },
                {
                    "NFE": 12600,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.176
                },
                {
                    "NFE": 18400,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.184
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                }
            ],
            "n4": [
                {
                    "NFE": 11200,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.016
                },
                {
                    "NFE": 15200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.0208
                },
                {
                    "NFE": 20200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 10500,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                }
            ],
            "n5": [
                {
                    "NFE": 11800,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                },
                {
                    "NFE": 13400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 22400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.00608
                },
                {
                    "NFE": 12250,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00224
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 10,
                    "real_peaks": 1,
                    "effective_number": 2,
                    "peak_accuracy": 3.7471784814794957,
                    "distance_accuracy": 1.3728607990781532
                },
                {
                    "NFE": 13800,
                    "number_of_peaks": 8,
                    "real_peaks": 2,
                    "effective_number": 1.6,
                    "peak_accuracy": 1.188416576612772,
                    "distance_accuracy": 3.1260964963031
                },
                {
                    "NFE": 20800,
                    "number_of_peaks": 16,
                    "real_peaks": 5,
                    "effective_number": 3.2,
                    "peak_accuracy": 2.9919389052617125,
                    "distance_accuracy": 2.03288244500484
                },
                {
                    "NFE": 10750,
                    "number_of_peaks": 12,
                    "real_peaks": 9,
                    "effective_number": 2.4,
                    "peak_accuracy": 3.087716253164206,
                    "distance_accuracy": 2.165956138338733
                }
            ],
            "n2": [
                {
                    "NFE": 10200,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                },
                {
                    "NFE": 17000,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                },
                {
                    "NFE": 16400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 15750,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                }
            ],
            "n3": [
                {
                    "NFE": 11600,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.096
                },
                {
                    "NFE": 11400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 27200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 11500,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.088
                }
            ],
            "n4": [
                {
                    "NFE": 13000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                },
                {
                    "NFE": 16800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.0272
                },
                {
                    "NFE": 28200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.0288
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                }
            ],
            "n5": [
                {
                    "NFE": 13600,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00288
                },
                {
                    "NFE": 15800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 29800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.00672
                },
                {
                    "NFE": 15750,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00224
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.8
                },
                {
                    "NFE": 11000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.8
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.2
                },
                {
                    "NFE": 10250,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.4
                }
            ],
            "n2": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.07
                },
                {
                    "NFE": 48400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.02
                },
                {
                    "NFE": 14600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.16
                },
                {
                    "NFE": 8500,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.02
                }
            ],
            "n3": [
                {
                    "NFE": 17000,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.003
                },
                {
                    "NFE": 13600,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.011
                },
                {
                    "NFE": 33600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.015
                },
                {
                    "NFE": 109500,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.002
                }
            ],
            "n4": [
                {
                    "NFE": 10800,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0006
                },
                {
                    "NFE": 28400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.0011
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.0019
                },
                {
                    "NFE": 34000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 37000,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.00004
                },
                {
                    "NFE": 37400,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00009
                },
                {
                    "NFE": 32800,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00008
                },
                {
                    "NFE": 23500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 12000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 1
                },
                {
                    "NFE": 16600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.8888888888888888
                },
                {
                    "NFE": 13600,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 2.111111111111111
                },
                {
                    "NFE": 8250,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 1.3333333333333333
                }
            ],
            "n2": [
                {
                    "NFE": 15200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.2962962962962963
                },
                {
                    "NFE": 24600,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.2222222222222222
                },
                {
                    "NFE": 20800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.7777777777777778
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.37037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 14200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.09876543209876543
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.20987654320987653
                },
                {
                    "NFE": 23200,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.25925925925925924
                },
                {
                    "NFE": 21750,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.07407407407407407
                }
            ],
            "n4": [
                {
                    "NFE": 14400,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 21400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.06172839506172839
                },
                {
                    "NFE": 37800,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.09465020576131687
                },
                {
                    "NFE": 21500,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.024691358024691357
                }
            ],
            "n5": [
                {
                    "NFE": 15000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.009602194787379973
                },
                {
                    "NFE": 21400,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.02880658436213992
                },
                {
                    "NFE": 40800,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.03155006858710562
                },
                {
                    "NFE": 16250,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.009602194787379973
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 10,
                    "real_peaks": 3,
                    "effective_number": 2,
                    "peak_accuracy": 4.258131451727362,
                    "distance_accuracy": 2.9666368083307106
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 23,
                    "real_peaks": 3,
                    "effective_number": 4.6,
                    "peak_accuracy": 0.6221398846507077,
                    "distance_accuracy": 4.42176838157461
                },
                {
                    "NFE": 20800,
                    "number_of_peaks": 12,
                    "real_peaks": 1,
                    "effective_number": 2.4,
                    "peak_accuracy": 2.998470467571691,
                    "distance_accuracy": 5.068207913564713
                },
                {
                    "NFE": 9250,
                    "number_of_peaks": 2,
                    "real_peaks": 1,
                    "effective_number": 0.4,
                    "peak_accuracy": 0.000301478849875747,
                    "distance_accuracy": 4.594831014963551
                }
            ],
            "n2": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.24
                },
                {
                    "NFE": 11400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.76
                },
                {
                    "NFE": 17400,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                },
                {
                    "NFE": 17250,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.24
                }
            ],
            "n3": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.096
                },
                {
                    "NFE": 11600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.12
                },
                {
                    "NFE": 22800,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 12250,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.04
                }
            ],
            "n4": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.0192
                },
                {
                    "NFE": 14800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                },
                {
                    "NFE": 20400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.0288
                },
                {
                    "NFE": 13500,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0096
                }
            ],
            "n5": [
                {
                    "NFE": 10000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.00384
                },
                {
                    "NFE": 12800,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.00512
                },
                {
                    "NFE": 24200,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.00672
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00256
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 8400,
                    "number_of_peaks": 11,
                    "real_peaks": 1,
                    "effective_number": 2.2,
                    "peak_accuracy": 3.176073011444947,
                    "distance_accuracy": 2.1181597161560126
                },
                {
                    "NFE": 8200,
                    "number_of_peaks": 20,
                    "real_peaks": 3,
                    "effective_number": 4,
                    "peak_accuracy": 3.857793520603982,
                    "distance_accuracy": 1.5639948597446738
                },
                {
                    "NFE": 24600,
                    "number_of_peaks": 14,
                    "real_peaks": 1,
                    "effective_number": 2.8,
                    "peak_accuracy": 2.295095395138333,
                    "distance_accuracy": 2.228033188170122
                },
                {
                    "NFE": 11500,
                    "number_of_peaks": 8,
                    "real_peaks": 1,
                    "effective_number": 1.6,
                    "peak_accuracy": 3.550631440812044,
                    "distance_accuracy": 1.4750621065392715
                }
            ],
            "n2": [
                {
                    "NFE": 13200,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.2
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.64
                },
                {
                    "NFE": 20800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.24
                }
            ],
            "n3": [
                {
                    "NFE": 12600,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.056
                },
                {
                    "NFE": 14400,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 20200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 13500,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                }
            ],
            "n4": [
                {
                    "NFE": 13800,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.0224
                },
                {
                    "NFE": 26400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 19500,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.0064
                }
            ],
            "n5": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.00416
                },
                {
                    "NFE": 24000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 38800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0048
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 9,
                    "real_peaks": 2,
                    "effective_number": 1.8,
                    "peak_accuracy": 1.4297883624315362,
                    "distance_accuracy": 4.369142078604585
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 11,
                    "real_peaks": 2,
                    "effective_number": 2.2,
                    "peak_accuracy": 2.000178833048626,
                    "distance_accuracy": 4.772301269103906
                },
                {
                    "NFE": 14600,
                    "number_of_peaks": 14,
                    "real_peaks": 4,
                    "effective_number": 2.8,
                    "peak_accuracy": 0.2709625336173208,
                    "distance_accuracy": 3.731302765109399
                },
                {
                    "NFE": 8500,
                    "number_of_peaks": 8,
                    "real_peaks": 2,
                    "effective_number": 1.6,
                    "peak_accuracy": 0.02881621049383487,
                    "distance_accuracy": 3.788312229918447
                }
            ],
            "n2": [
                {
                    "NFE": 9800,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.44
                },
                {
                    "NFE": 11800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 12400,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.88
                },
                {
                    "NFE": 9500,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                }
            ],
            "n3": [
                {
                    "NFE": 8600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 13800,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 16400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 11000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.096
                }
            ],
            "n4": [
                {
                    "NFE": 12800,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.0112
                },
                {
                    "NFE": 14800,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 22800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 10750,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.016
                }
            ],
            "n5": [
                {
                    "NFE": 13600,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 16800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 24600,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.00736
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00288
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 8000,
                    "number_of_peaks": 11,
                    "real_peaks": 4,
                    "effective_number": 2.2,
                    "peak_accuracy": 3.370715649042869,
                    "distance_accuracy": 2.1534325320184258
                },
                {
                    "NFE": 9200,
                    "number_of_peaks": 16,
                    "real_peaks": 7,
                    "effective_number": 3.2,
                    "peak_accuracy": 3.338059945549337,
                    "distance_accuracy": 3.0824006652409497
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 17,
                    "real_peaks": 1,
                    "effective_number": 3.4,
                    "peak_accuracy": 3.6234268153413054,
                    "distance_accuracy": 1.3722990430886903
                },
                {
                    "NFE": 8750,
                    "number_of_peaks": 10,
                    "real_peaks": 3,
                    "effective_number": 2,
                    "peak_accuracy": 1.0934449539932816,
                    "distance_accuracy": 3.1553732705320328
                }
            ],
            "n2": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.36
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.6
                },
                {
                    "NFE": 21600,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.72
                },
                {
                    "NFE": 17000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                }
            ],
            "n3": [
                {
                    "NFE": 13000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.064
                },
                {
                    "NFE": 14600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.12
                },
                {
                    "NFE": 18400,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.168
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.08
                }
            ],
            "n4": [
                {
                    "NFE": 10200,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.0176
                },
                {
                    "NFE": 14600,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.0288
                },
                {
                    "NFE": 18200,
                    "number_of_peaks": 28,
                    "real_peaks": 28,
                    "effective_number": 0.0448
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                }
            ],
            "n5": [
                {
                    "NFE": 15000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 14600,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 29400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.00608
                },
                {
                    "NFE": 16250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00256
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 10600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.8
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 1.4
                },
                {
                    "NFE": 15800,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 1
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.3
                }
            ],
            "n2": [
                {
                    "NFE": 13600,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.06
                },
                {
                    "NFE": 21200,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.04
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.11
                },
                {
                    "NFE": 61000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 13200,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.006
                },
                {
                    "NFE": 32800,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.01
                },
                {
                    "NFE": 17600,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.014
                },
                {
                    "NFE": 48250,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.002
                }
            ],
            "n4": [
                {
                    "NFE": 17800,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0006
                },
                {
                    "NFE": 53000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 16800,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.0014
                },
                {
                    "NFE": 77500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 50200,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.00006
                },
                {
                    "NFE": 13600,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.00013
                },
                {
                    "NFE": 42200,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.00003
                },
                {
                    "NFE": 35250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 13400,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.7777777777777778
                },
                {
                    "NFE": 14800,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 1.3333333333333333
                },
                {
                    "NFE": 25000,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 1.4444444444444444
                },
                {
                    "NFE": 10500,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.3333333333333333
                }
            ],
            "n2": [
                {
                    "NFE": 13800,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.25925925925925924
                },
                {
                    "NFE": 20600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.5555555555555556
                },
                {
                    "NFE": 27400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.7407407407407407
                },
                {
                    "NFE": 22250,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.2222222222222222
                }
            ],
            "n3": [
                {
                    "NFE": 14400,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.09876543209876543
                },
                {
                    "NFE": 20600,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.14814814814814814
                },
                {
                    "NFE": 27400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.18518518518518517
                },
                {
                    "NFE": 14750,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.13580246913580246
                }
            ],
            "n4": [
                {
                    "NFE": 15400,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.03292181069958848
                },
                {
                    "NFE": 24200,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.06172839506172839
                },
                {
                    "NFE": 25800,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.07818930041152264
                },
                {
                    "NFE": 17750,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.0205761316872428
                }
            ],
            "n5": [
                {
                    "NFE": 14400,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 23800,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.019204389574759947
                },
                {
                    "NFE": 35600,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.02880658436213992
                },
                {
                    "NFE": 11750,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.01646090534979424
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 6,
                    "real_peaks": 1,
                    "effective_number": 1.2,
                    "peak_accuracy": 1.0472226662562774,
                    "distance_accuracy": 3.986587808625683
                },
                {
                    "NFE": 9200,
                    "number_of_peaks": 8,
                    "real_peaks": 3,
                    "effective_number": 1.6,
                    "peak_accuracy": 2.9999544557728504,
                    "distance_accuracy": 5.270010697750352
                },
                {
                    "NFE": 20000,
                    "number_of_peaks": 10,
                    "real_peaks": 2,
                    "effective_number": 2,
                    "peak_accuracy": 1.7076743671640848,
                    "distance_accuracy": 4.601776778224201
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00005905553251384088,
                    "distance_accuracy": 3.976361295175946
                }
            ],
            "n2": [
                {
                    "NFE": 7600,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.76
                },
                {
                    "NFE": 8800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 24800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 11000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                }
            ],
            "n3": [
                {
                    "NFE": 12600,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.04
                },
                {
                    "NFE": 10000,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.152
                },
                {
                    "NFE": 14600,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.064
                }
            ],
            "n4": [
                {
                    "NFE": 14200,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.0112
                },
                {
                    "NFE": 19400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                },
                {
                    "NFE": 25200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 15750,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.008
                }
            ],
            "n5": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.00384
                },
                {
                    "NFE": 13600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0048
                },
                {
                    "NFE": 24800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 15500,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 8600,
                    "number_of_peaks": 10,
                    "real_peaks": 1,
                    "effective_number": 2,
                    "peak_accuracy": 3.219853436109825,
                    "distance_accuracy": 1.9260811930461654
                },
                {
                    "NFE": 12600,
                    "number_of_peaks": 10,
                    "real_peaks": 3,
                    "effective_number": 2,
                    "peak_accuracy": 3.2398570462783227,
                    "distance_accuracy": 1.2146979119584618
                },
                {
                    "NFE": 15600,
                    "number_of_peaks": 12,
                    "real_peaks": 1,
                    "effective_number": 2.4,
                    "peak_accuracy": 3.8483088820817146,
                    "distance_accuracy": 1.6056454739158352
                },
                {
                    "NFE": 10750,
                    "number_of_peaks": 10,
                    "real_peaks": 4,
                    "effective_number": 2,
                    "peak_accuracy": 2.5342671856789125,
                    "distance_accuracy": 1.7925749266215867
                }
            ],
            "n2": [
                {
                    "NFE": 12600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                },
                {
                    "NFE": 15800,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.48
                },
                {
                    "NFE": 21200,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.76
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                }
            ],
            "n3": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.056
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 20000,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.152
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.064
                }
            ],
            "n4": [
                {
                    "NFE": 13000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                },
                {
                    "NFE": 16400,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 24800,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.0352
                },
                {
                    "NFE": 17750,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                }
            ],
            "n5": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00288
                },
                {
                    "NFE": 17600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 27600,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.00576
                },
                {
                    "NFE": 16750,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00224
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 9200,
                    "number_of_peaks": 8,
                    "real_peaks": 1,
                    "effective_number": 1.6,
                    "peak_accuracy": 0.9450436384836887,
                    "distance_accuracy": 4.207259415370525
                },
                {
                    "NFE": 9200,
                    "number_of_peaks": 8,
                    "real_peaks": 1,
                    "effective_number": 1.6,
                    "peak_accuracy": 0.7056660615934048,
                    "distance_accuracy": 4.042286934311636
                },
                {
                    "NFE": 7400,
                    "number_of_peaks": 11,
                    "real_peaks": 3,
                    "effective_number": 2.2,
                    "peak_accuracy": 3.8958656889588403,
                    "distance_accuracy": 2.714562754251192
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 9,
                    "real_peaks": 3,
                    "effective_number": 1.8,
                    "peak_accuracy": 0.028781293023813048,
                    "distance_accuracy": 4.103879737867328
                }
            ],
            "n2": [
                {
                    "NFE": 9000,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                },
                {
                    "NFE": 11200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 10200,
                    "number_of_peaks": 24,
                    "real_peaks": 24,
                    "effective_number": 0.96
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.24
                }
            ],
            "n3": [
                {
                    "NFE": 11200,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.088
                },
                {
                    "NFE": 12800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 24800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 12500,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.064
                }
            ],
            "n4": [
                {
                    "NFE": 10600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                },
                {
                    "NFE": 18200,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.0224
                },
                {
                    "NFE": 19000,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.0336
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.0112
                }
            ],
            "n5": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 19800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0048
                },
                {
                    "NFE": 24800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.00672
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.00384
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 10800,
                    "number_of_peaks": 10,
                    "real_peaks": 2,
                    "effective_number": 2,
                    "peak_accuracy": 3.1431481578362597,
                    "distance_accuracy": 2.8852339772294977
                },
                {
                    "NFE": 9800,
                    "number_of_peaks": 10,
                    "real_peaks": 4,
                    "effective_number": 2,
                    "peak_accuracy": 3.9076583665317526,
                    "distance_accuracy": 1.5941001663721446
                },
                {
                    "NFE": 15800,
                    "number_of_peaks": 10,
                    "real_peaks": 1,
                    "effective_number": 2,
                    "peak_accuracy": 3.2153069412360784,
                    "distance_accuracy": 2.0978062307436565
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 7,
                    "real_peaks": 4,
                    "effective_number": 1.4,
                    "peak_accuracy": 1.9672818204337497,
                    "distance_accuracy": 3.174304582602411
                }
            ],
            "n2": [
                {
                    "NFE": 13000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.6
                },
                {
                    "NFE": 17800,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.76
                },
                {
                    "NFE": 11750,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.36
                }
            ],
            "n3": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 14600,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 15600,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.176
                },
                {
                    "NFE": 14750,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.04
                }
            ],
            "n4": [
                {
                    "NFE": 14000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                },
                {
                    "NFE": 21400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.0176
                },
                {
                    "NFE": 22800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.0336
                },
                {
                    "NFE": 16250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.016
                }
            ],
            "n5": [
                {
                    "NFE": 11800,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                },
                {
                    "NFE": 13200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 24800,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.00704
                },
                {
                    "NFE": 19250,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.00128
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 10800,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.7
                },
                {
                    "NFE": 10200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 1.3
                },
                {
                    "NFE": 10000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 1
                },
                {
                    "NFE": 11250,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.4
                }
            ],
            "n2": [
                {
                    "NFE": 7400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.15
                },
                {
                    "NFE": 19400,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.03
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.17
                },
                {
                    "NFE": 12500,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.07
                }
            ],
            "n3": [
                {
                    "NFE": 33600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.002
                },
                {
                    "NFE": 22400,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.004
                },
                {
                    "NFE": 18600,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.012
                },
                {
                    "NFE": 43500,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.002
                }
            ],
            "n4": [
                {
                    "NFE": 15200,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.0007
                },
                {
                    "NFE": 25600,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.0005
                },
                {
                    "NFE": 44600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 136750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 13000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00009
                },
                {
                    "NFE": 43200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00008
                },
                {
                    "NFE": 47000,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.00003
                },
                {
                    "NFE": 61000,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.00004
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 7800,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 1.5555555555555556
                },
                {
                    "NFE": 12600,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.7777777777777778
                },
                {
                    "NFE": 22200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 1.4444444444444444
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.6666666666666666
                }
            ],
            "n2": [
                {
                    "NFE": 8200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.6666666666666666
                },
                {
                    "NFE": 15600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.5555555555555556
                },
                {
                    "NFE": 25600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.5555555555555556
                },
                {
                    "NFE": 14750,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.5185185185185185
                }
            ],
            "n3": [
                {
                    "NFE": 12600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.09876543209876543
                },
                {
                    "NFE": 15400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.24691358024691357
                },
                {
                    "NFE": 26000,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.2839506172839506
                },
                {
                    "NFE": 22000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.09876543209876543
                }
            ],
            "n4": [
                {
                    "NFE": 11800,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 20400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.07407407407407407
                },
                {
                    "NFE": 47600,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.05761316872427984
                },
                {
                    "NFE": 16250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.03292181069958848
                }
            ],
            "n5": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 24000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.02194787379972565
                },
                {
                    "NFE": 41800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0205761316872428
                },
                {
                    "NFE": 24000,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.006858710562414266
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 7800,
                    "number_of_peaks": 9,
                    "real_peaks": 3,
                    "effective_number": 1.8,
                    "peak_accuracy": 2.158650226017417,
                    "distance_accuracy": 4.653042940589769
                },
                {
                    "NFE": 9200,
                    "number_of_peaks": 11,
                    "real_peaks": 2,
                    "effective_number": 2.2,
                    "peak_accuracy": 3.0015707073843245,
                    "distance_accuracy": 5.251644194988358
                },
                {
                    "NFE": 19200,
                    "number_of_peaks": 9,
                    "real_peaks": 4,
                    "effective_number": 1.8,
                    "peak_accuracy": 2.7941807656238415,
                    "distance_accuracy": 2.568836924783973
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 7,
                    "real_peaks": 2,
                    "effective_number": 1.4,
                    "peak_accuracy": 0.01801984946794344,
                    "distance_accuracy": 3.9577171807230913
                }
            ],
            "n2": [
                {
                    "NFE": 15400,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.24
                },
                {
                    "NFE": 10200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 13200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.64
                },
                {
                    "NFE": 12500,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.16
                }
            ],
            "n3": [
                {
                    "NFE": 14400,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.056
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.104
                },
                {
                    "NFE": 29400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.096
                },
                {
                    "NFE": 10500,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.096
                }
            ],
            "n4": [
                {
                    "NFE": 8800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                },
                {
                    "NFE": 11800,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.0304
                },
                {
                    "NFE": 26400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 17000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0096
                }
            ],
            "n5": [
                {
                    "NFE": 12000,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 13400,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.00512
                },
                {
                    "NFE": 24600,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.00576
                },
                {
                    "NFE": 18000,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.00128
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 9400,
                    "number_of_peaks": 10,
                    "real_peaks": 3,
                    "effective_number": 2,
                    "peak_accuracy": 3.896691335149984,
                    "distance_accuracy": 1.6301177326472664
                },
                {
                    "NFE": 11200,
                    "number_of_peaks": 16,
                    "real_peaks": 3,
                    "effective_number": 3.2,
                    "peak_accuracy": 3.188325699579424,
                    "distance_accuracy": 1.2508399601987865
                },
                {
                    "NFE": 21200,
                    "number_of_peaks": 17,
                    "real_peaks": 5,
                    "effective_number": 3.4,
                    "peak_accuracy": 2.6560516565744057,
                    "distance_accuracy": 2.559383794654754
                },
                {
                    "NFE": 10000,
                    "number_of_peaks": 9,
                    "real_peaks": 1,
                    "effective_number": 1.8,
                    "peak_accuracy": 2.633812239450972,
                    "distance_accuracy": 2.153460346822914
                }
            ],
            "n2": [
                {
                    "NFE": 11800,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.24
                },
                {
                    "NFE": 14800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 21400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.76
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                }
            ],
            "n3": [
                {
                    "NFE": 13400,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                },
                {
                    "NFE": 16400,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 19800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                }
            ],
            "n4": [
                {
                    "NFE": 12000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.0112
                },
                {
                    "NFE": 15200,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                },
                {
                    "NFE": 23400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.0288
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.016
                }
            ],
            "n5": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 26400,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.00672
                },
                {
                    "NFE": 16500,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00224
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 7200,
                    "number_of_peaks": 13,
                    "real_peaks": 2,
                    "effective_number": 2.6,
                    "peak_accuracy": 2.0010821974439907,
                    "distance_accuracy": 4.785679294726799
                },
                {
                    "NFE": 10200,
                    "number_of_peaks": 11,
                    "real_peaks": 2,
                    "effective_number": 2.2,
                    "peak_accuracy": 1.349347352496943,
                    "distance_accuracy": 4.270400717214931
                },
                {
                    "NFE": 15200,
                    "number_of_peaks": 10,
                    "real_peaks": 3,
                    "effective_number": 2,
                    "peak_accuracy": 0.09520072720952188,
                    "distance_accuracy": 4.7370415525860885
                },
                {
                    "NFE": 9250,
                    "number_of_peaks": 11,
                    "real_peaks": 5,
                    "effective_number": 2.2,
                    "peak_accuracy": 0.034481345437254896,
                    "distance_accuracy": 3.7894161799965667
                }
            ],
            "n2": [
                {
                    "NFE": 8400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.44
                },
                {
                    "NFE": 13400,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.56
                },
                {
                    "NFE": 18000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 11250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                }
            ],
            "n3": [
                {
                    "NFE": 14400,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.056
                },
                {
                    "NFE": 13800,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 20000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                }
            ],
            "n4": [
                {
                    "NFE": 13800,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                },
                {
                    "NFE": 11200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.0288
                },
                {
                    "NFE": 20000,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.008
                }
            ],
            "n5": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.00512
                },
                {
                    "NFE": 14400,
                    "number_of_peaks": 24,
                    "real_peaks": 24,
                    "effective_number": 0.00768
                },
                {
                    "NFE": 13500,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 5,
                    "real_peaks": 2,
                    "effective_number": 1,
                    "peak_accuracy": 2.393758757811087,
                    "distance_accuracy": 2.843812576502267
                },
                {
                    "NFE": 12800,
                    "number_of_peaks": 15,
                    "real_peaks": 1,
                    "effective_number": 3,
                    "peak_accuracy": 3.7637153998820914,
                    "distance_accuracy": 1.3853264919759156
                },
                {
                    "NFE": 14200,
                    "number_of_peaks": 21,
                    "real_peaks": 4,
                    "effective_number": 4.2,
                    "peak_accuracy": 2.685382709269968,
                    "distance_accuracy": 1.5185135633516333
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 4,
                    "real_peaks": 1,
                    "effective_number": 0.8,
                    "peak_accuracy": 1.8205583029236572,
                    "distance_accuracy": 3.2638301178606803
                }
            ],
            "n2": [
                {
                    "NFE": 13600,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.36
                },
                {
                    "NFE": 15200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.64
                },
                {
                    "NFE": 20000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.6
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                }
            ],
            "n3": [
                {
                    "NFE": 11200,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.088
                },
                {
                    "NFE": 11400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 20200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 15500,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                }
            ],
            "n4": [
                {
                    "NFE": 13200,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                },
                {
                    "NFE": 16400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.0192
                },
                {
                    "NFE": 20800,
                    "number_of_peaks": 27,
                    "real_peaks": 27,
                    "effective_number": 0.0432
                },
                {
                    "NFE": 19500,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.008
                }
            ],
            "n5": [
                {
                    "NFE": 10800,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                },
                {
                    "NFE": 16600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 22000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.00512
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 13200,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.9
                },
                {
                    "NFE": 8800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 1.7
                },
                {
                    "NFE": 13200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 1.3
                },
                {
                    "NFE": 10250,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 1.3
                }
            ],
            "n2": [
                {
                    "NFE": 13200,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.1
                },
                {
                    "NFE": 56000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.07
                },
                {
                    "NFE": 23400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.17
                },
                {
                    "NFE": 38750,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.02
                }
            ],
            "n3": [
                {
                    "NFE": 14600,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.01
                },
                {
                    "NFE": 27000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.002
                },
                {
                    "NFE": 19600,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.009
                },
                {
                    "NFE": 12250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 21200,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.0004
                },
                {
                    "NFE": 55200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 61000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.0007
                },
                {
                    "NFE": 75250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00007
                },
                {
                    "NFE": 17200,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 12800,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.00004
                },
                {
                    "NFE": 62500,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00002
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 1
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 1.6666666666666667
                },
                {
                    "NFE": 18200,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 2.3333333333333335
                },
                {
                    "NFE": 11750,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 1
                }
            ],
            "n2": [
                {
                    "NFE": 13800,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.25925925925925924
                },
                {
                    "NFE": 21400,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.3333333333333333
                },
                {
                    "NFE": 29000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.5555555555555556
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.37037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 15000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.09876543209876543
                },
                {
                    "NFE": 22400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.18518518518518517
                },
                {
                    "NFE": 34400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.2222222222222222
                },
                {
                    "NFE": 20500,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.07407407407407407
                }
            ],
            "n4": [
                {
                    "NFE": 13800,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.03292181069958848
                },
                {
                    "NFE": 22000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.06584362139917696
                },
                {
                    "NFE": 28000,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.053497942386831275
                },
                {
                    "NFE": 19500,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n5": [
                {
                    "NFE": 10200,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.019204389574759947
                },
                {
                    "NFE": 24200,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0205761316872428
                },
                {
                    "NFE": 35000,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 18250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.013717421124828532
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 10800,
                    "number_of_peaks": 8,
                    "real_peaks": 3,
                    "effective_number": 1.6,
                    "peak_accuracy": 0.0023021133929292947,
                    "distance_accuracy": 3.976279124042954
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 10,
                    "real_peaks": 3,
                    "effective_number": 2,
                    "peak_accuracy": 3.0124378431780165,
                    "distance_accuracy": 5.281077398343193
                },
                {
                    "NFE": 10600,
                    "number_of_peaks": 8,
                    "real_peaks": 3,
                    "effective_number": 1.6,
                    "peak_accuracy": 2.9813287820207455,
                    "distance_accuracy": 5.130508990011979
                },
                {
                    "NFE": 9250,
                    "number_of_peaks": 4,
                    "real_peaks": 1,
                    "effective_number": 0.8,
                    "peak_accuracy": 2.471250534208072,
                    "distance_accuracy": 4.885081909181176
                }
            ],
            "n2": [
                {
                    "NFE": 11600,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.44
                },
                {
                    "NFE": 14400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 18600,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.76
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.24
                }
            ],
            "n3": [
                {
                    "NFE": 14200,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                },
                {
                    "NFE": 9200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 23200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 16250,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                }
            ],
            "n4": [
                {
                    "NFE": 9400,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.0224
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 24000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.0272
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.016
                }
            ],
            "n5": [
                {
                    "NFE": 14200,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.00192
                },
                {
                    "NFE": 13200,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.00736
                },
                {
                    "NFE": 25200,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.00608
                },
                {
                    "NFE": 11250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 10600,
                    "number_of_peaks": 8,
                    "real_peaks": 2,
                    "effective_number": 1.6,
                    "peak_accuracy": 3.8575897939584576,
                    "distance_accuracy": 1.564323593163037
                },
                {
                    "NFE": 12800,
                    "number_of_peaks": 12,
                    "real_peaks": 6,
                    "effective_number": 2.4,
                    "peak_accuracy": 2.913054372494871,
                    "distance_accuracy": 1.9613689232393998
                },
                {
                    "NFE": 14600,
                    "number_of_peaks": 18,
                    "real_peaks": 3,
                    "effective_number": 3.6,
                    "peak_accuracy": 3.3326128459935545,
                    "distance_accuracy": 6.061743213175699
                },
                {
                    "NFE": 10750,
                    "number_of_peaks": 8,
                    "real_peaks": 3,
                    "effective_number": 1.6,
                    "peak_accuracy": 2.973636881933036,
                    "distance_accuracy": 1.6579107299707987
                }
            ],
            "n2": [
                {
                    "NFE": 12000,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.44
                },
                {
                    "NFE": 13400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 27800,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.72
                },
                {
                    "NFE": 15250,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.24
                }
            ],
            "n3": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.064
                },
                {
                    "NFE": 13800,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.176
                },
                {
                    "NFE": 23800,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 11750,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.056
                }
            ],
            "n4": [
                {
                    "NFE": 14000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0096
                },
                {
                    "NFE": 17000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 22400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 12750,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.0192
                }
            ],
            "n5": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.00448
                },
                {
                    "NFE": 14200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.00512
                },
                {
                    "NFE": 25200,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.00704
                },
                {
                    "NFE": 17500,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00256
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 9600,
                    "number_of_peaks": 7,
                    "real_peaks": 2,
                    "effective_number": 1.4,
                    "peak_accuracy": 3.3430959025315325,
                    "distance_accuracy": 5.098123515103858
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 18,
                    "real_peaks": 4,
                    "effective_number": 3.6,
                    "peak_accuracy": 0.7512752781486156,
                    "distance_accuracy": 4.157465189748955
                },
                {
                    "NFE": 16600,
                    "number_of_peaks": 10,
                    "real_peaks": 5,
                    "effective_number": 2,
                    "peak_accuracy": 0.41358113037224764,
                    "distance_accuracy": 4.113415773610592
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 7,
                    "real_peaks": 4,
                    "effective_number": 1.4,
                    "peak_accuracy": 2.866251544916256,
                    "distance_accuracy": 3.362223103833328
                }
            ],
            "n2": [
                {
                    "NFE": 9000,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.56
                },
                {
                    "NFE": 10000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 17600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.64
                },
                {
                    "NFE": 11750,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.44
                }
            ],
            "n3": [
                {
                    "NFE": 9200,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.08
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.12
                },
                {
                    "NFE": 23200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 11500,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                }
            ],
            "n4": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                },
                {
                    "NFE": 15800,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 27600,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.0304
                },
                {
                    "NFE": 20000,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.0064
                }
            ],
            "n5": [
                {
                    "NFE": 9000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0048
                },
                {
                    "NFE": 18800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 18750,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.00192
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 11600,
                    "number_of_peaks": 8,
                    "real_peaks": 3,
                    "effective_number": 1.6,
                    "peak_accuracy": 3.1291754837973738,
                    "distance_accuracy": 1.17012860098462
                },
                {
                    "NFE": 15600,
                    "number_of_peaks": 12,
                    "real_peaks": 2,
                    "effective_number": 2.4,
                    "peak_accuracy": 2.863641280666344,
                    "distance_accuracy": 1.2814380918349566
                },
                {
                    "NFE": 19000,
                    "number_of_peaks": 8,
                    "real_peaks": 2,
                    "effective_number": 1.6,
                    "peak_accuracy": 3.3717694975103125,
                    "distance_accuracy": 2.5710123430766156
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 8,
                    "real_peaks": 3,
                    "effective_number": 1.6,
                    "peak_accuracy": 3.107225849378801,
                    "distance_accuracy": 4.329412877650166
                }
            ],
            "n2": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.2
                },
                {
                    "NFE": 12800,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.48
                },
                {
                    "NFE": 16800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.84
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                }
            ],
            "n3": [
                {
                    "NFE": 11200,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.096
                },
                {
                    "NFE": 12400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.12
                },
                {
                    "NFE": 16800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 16500,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                }
            ],
            "n4": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.0176
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.0304
                },
                {
                    "NFE": 26200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.016
                }
            ],
            "n5": [
                {
                    "NFE": 14400,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.00192
                },
                {
                    "NFE": 14200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.00576
                },
                {
                    "NFE": 22800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.00672
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00224
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 13600,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.5
                },
                {
                    "NFE": 12800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 1.9
                },
                {
                    "NFE": 29250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 9400,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.14
                },
                {
                    "NFE": 48800,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.05
                },
                {
                    "NFE": 26600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.17
                },
                {
                    "NFE": 33500,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.03
                }
            ],
            "n3": [
                {
                    "NFE": 8800,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.016
                },
                {
                    "NFE": 53400,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.006
                },
                {
                    "NFE": 30000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.002
                },
                {
                    "NFE": 21250,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.002
                }
            ],
            "n4": [
                {
                    "NFE": 25400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0002
                },
                {
                    "NFE": 10600,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.0013
                },
                {
                    "NFE": 44400,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0006
                },
                {
                    "NFE": 96250,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0002
                }
            ],
            "n5": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 37000,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.00003
                },
                {
                    "NFE": 17600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.00015
                },
                {
                    "NFE": 18000,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.00003
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 9800,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 1.1111111111111112
                },
                {
                    "NFE": 10200,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 1.6666666666666667
                },
                {
                    "NFE": 23600,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 2.3333333333333335
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.7777777777777778
                }
            ],
            "n2": [
                {
                    "NFE": 11200,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.37037037037037035
                },
                {
                    "NFE": 22400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.4074074074074074
                },
                {
                    "NFE": 25000,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.6666666666666666
                },
                {
                    "NFE": 18000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.3333333333333333
                }
            ],
            "n3": [
                {
                    "NFE": 14000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 18400,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.25925925925925924
                },
                {
                    "NFE": 32800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.20987654320987653
                },
                {
                    "NFE": 17250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.12345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 14200,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 18400,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.053497942386831275
                },
                {
                    "NFE": 29800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0823045267489712
                },
                {
                    "NFE": 16750,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n5": [
                {
                    "NFE": 13000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.010973936899862825
                },
                {
                    "NFE": 20000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.01646090534979424
                },
                {
                    "NFE": 31200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.023319615912208505
                },
                {
                    "NFE": 23750,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.00411522633744856
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 9600,
                    "number_of_peaks": 7,
                    "real_peaks": 2,
                    "effective_number": 1.4,
                    "peak_accuracy": 2.9961301736153207,
                    "distance_accuracy": 5.202364828801592
                },
                {
                    "NFE": 7800,
                    "number_of_peaks": 13,
                    "real_peaks": 3,
                    "effective_number": 2.6,
                    "peak_accuracy": 2.983504876827772,
                    "distance_accuracy": 5.21638754434104
                },
                {
                    "NFE": 19800,
                    "number_of_peaks": 13,
                    "real_peaks": 3,
                    "effective_number": 2.6,
                    "peak_accuracy": 3.0091835371110625,
                    "distance_accuracy": 2.757374161615491
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 4,
                    "real_peaks": 1,
                    "effective_number": 0.8,
                    "peak_accuracy": 1.5868898888617344,
                    "distance_accuracy": 4.626094103877976
                }
            ],
            "n2": [
                {
                    "NFE": 8800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.6
                },
                {
                    "NFE": 12200,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.6
                },
                {
                    "NFE": 16600,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                },
                {
                    "NFE": 10500,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.36
                }
            ],
            "n3": [
                {
                    "NFE": 10000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 19200,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.152
                },
                {
                    "NFE": 11750,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.08
                }
            ],
            "n4": [
                {
                    "NFE": 12600,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.0176
                },
                {
                    "NFE": 15200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 24200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 12500,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.0208
                }
            ],
            "n5": [
                {
                    "NFE": 11600,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.00384
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.00576
                },
                {
                    "NFE": 20600,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.00608
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00224
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 10000,
                    "number_of_peaks": 9,
                    "real_peaks": 1,
                    "effective_number": 1.8,
                    "peak_accuracy": 2.4289179970815185,
                    "distance_accuracy": 3.3729177884838806
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 13,
                    "real_peaks": 5,
                    "effective_number": 2.6,
                    "peak_accuracy": 2.6988061461595274,
                    "distance_accuracy": 2.059058446188575
                },
                {
                    "NFE": 11800,
                    "number_of_peaks": 17,
                    "real_peaks": 5,
                    "effective_number": 3.4,
                    "peak_accuracy": 3.4202784910841233,
                    "distance_accuracy": 1.5960940993398094
                },
                {
                    "NFE": 9250,
                    "number_of_peaks": 11,
                    "real_peaks": 2,
                    "effective_number": 2.2,
                    "peak_accuracy": 2.6531442012200492,
                    "distance_accuracy": 1.5632937229483617
                }
            ],
            "n2": [
                {
                    "NFE": 14200,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.24
                },
                {
                    "NFE": 12200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.72
                },
                {
                    "NFE": 24400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.44
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                }
            ],
            "n3": [
                {
                    "NFE": 13200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.064
                },
                {
                    "NFE": 19400,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.112
                },
                {
                    "NFE": 22400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.152
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                }
            ],
            "n4": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.0192
                },
                {
                    "NFE": 13600,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.0352
                },
                {
                    "NFE": 26400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.0304
                },
                {
                    "NFE": 14750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                }
            ],
            "n5": [
                {
                    "NFE": 11600,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00288
                },
                {
                    "NFE": 19600,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.00416
                },
                {
                    "NFE": 37000,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.00576
                },
                {
                    "NFE": 17500,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00288
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 9600,
                    "number_of_peaks": 8,
                    "real_peaks": 1,
                    "effective_number": 1.6,
                    "peak_accuracy": 2.0869443343489573,
                    "distance_accuracy": 4.686716565394083
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 17,
                    "real_peaks": 4,
                    "effective_number": 3.4,
                    "peak_accuracy": 0.007510970091788027,
                    "distance_accuracy": 3.806993476779464
                },
                {
                    "NFE": 10400,
                    "number_of_peaks": 18,
                    "real_peaks": 4,
                    "effective_number": 3.6,
                    "peak_accuracy": 2.1163878393453595,
                    "distance_accuracy": 4.662926719162908
                },
                {
                    "NFE": 8500,
                    "number_of_peaks": 9,
                    "real_peaks": 3,
                    "effective_number": 1.8,
                    "peak_accuracy": 0.18440015833422596,
                    "distance_accuracy": 4.6894280446891
                }
            ],
            "n2": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                },
                {
                    "NFE": 12400,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.56
                },
                {
                    "NFE": 13200,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.92
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.36
                }
            ],
            "n3": [
                {
                    "NFE": 10200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.104
                },
                {
                    "NFE": 14800,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.112
                },
                {
                    "NFE": 21400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 10750,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.088
                }
            ],
            "n4": [
                {
                    "NFE": 9600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                },
                {
                    "NFE": 11400,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.0368
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.0352
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                }
            ],
            "n5": [
                {
                    "NFE": 11200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00256
                },
                {
                    "NFE": 12400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.00608
                },
                {
                    "NFE": 20400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.00608
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.00384
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 11800,
                    "number_of_peaks": 7,
                    "real_peaks": 1,
                    "effective_number": 1.4,
                    "peak_accuracy": 3.881378457556715,
                    "distance_accuracy": 1.5686138798304607
                },
                {
                    "NFE": 10600,
                    "number_of_peaks": 14,
                    "real_peaks": 2,
                    "effective_number": 2.8,
                    "peak_accuracy": 3.691617005371305,
                    "distance_accuracy": 1.3359642703212888
                },
                {
                    "NFE": 12400,
                    "number_of_peaks": 10,
                    "real_peaks": 3,
                    "effective_number": 2,
                    "peak_accuracy": 2.348359720162378,
                    "distance_accuracy": 2.3171570825009704
                },
                {
                    "NFE": 14500,
                    "number_of_peaks": 3,
                    "real_peaks": 2,
                    "effective_number": 0.6,
                    "peak_accuracy": 2.0341006633755154,
                    "distance_accuracy": 3.0883237301934217
                }
            ],
            "n2": [
                {
                    "NFE": 13400,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                },
                {
                    "NFE": 13200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                },
                {
                    "NFE": 19000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 17500,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                }
            ],
            "n3": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.08
                },
                {
                    "NFE": 11800,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.152
                },
                {
                    "NFE": 23600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.056
                }
            ],
            "n4": [
                {
                    "NFE": 12000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.0192
                },
                {
                    "NFE": 16600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 27800,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.0288
                },
                {
                    "NFE": 15500,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.008
                }
            ],
            "n5": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.00384
                },
                {
                    "NFE": 19200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.00512
                },
                {
                    "NFE": 22200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 9600,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 1.2
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.3
                },
                {
                    "NFE": 10800,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 1.2
                },
                {
                    "NFE": 11000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.9
                }
            ],
            "n2": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.08
                },
                {
                    "NFE": 20400,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.09
                },
                {
                    "NFE": 14600,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.22
                },
                {
                    "NFE": 19250,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.03
                }
            ],
            "n3": [
                {
                    "NFE": 12800,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.006
                },
                {
                    "NFE": 35000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.002
                },
                {
                    "NFE": 30400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.015
                },
                {
                    "NFE": 13500,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.005
                }
            ],
            "n4": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.001
                },
                {
                    "NFE": 68400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 35800,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0008
                },
                {
                    "NFE": 138000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 26000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00007
                },
                {
                    "NFE": 9000,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.00014
                },
                {
                    "NFE": 29600,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00007
                },
                {
                    "NFE": 91000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00002
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 1.1111111111111112
                },
                {
                    "NFE": 15400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 1.2222222222222223
                },
                {
                    "NFE": 24200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 2
                },
                {
                    "NFE": 11000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.6666666666666666
                }
            ],
            "n2": [
                {
                    "NFE": 10600,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.4444444444444444
                },
                {
                    "NFE": 19200,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.5555555555555556
                },
                {
                    "NFE": 26000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.6296296296296297
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.25925925925925924
                }
            ],
            "n3": [
                {
                    "NFE": 13400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.14814814814814814
                },
                {
                    "NFE": 24200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.20987654320987653
                },
                {
                    "NFE": 39000,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.2222222222222222
                },
                {
                    "NFE": 13500,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.08641975308641975
                }
            ],
            "n4": [
                {
                    "NFE": 11800,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.04526748971193416
                },
                {
                    "NFE": 24800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.06995884773662552
                },
                {
                    "NFE": 24200,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.08641975308641975
                },
                {
                    "NFE": 20500,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.024691358024691357
                }
            ],
            "n5": [
                {
                    "NFE": 12600,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.015089163237311385
                },
                {
                    "NFE": 23200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.02194787379972565
                },
                {
                    "NFE": 36600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0205761316872428
                },
                {
                    "NFE": 20500,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.00823045267489712
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 8400,
                    "number_of_peaks": 12,
                    "real_peaks": 1,
                    "effective_number": 2.4,
                    "peak_accuracy": 3.170121954710014,
                    "distance_accuracy": 2.445155884783353
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 12,
                    "real_peaks": 4,
                    "effective_number": 2.4,
                    "peak_accuracy": 2.8050370345507396,
                    "distance_accuracy": 3.9188175059564507
                },
                {
                    "NFE": 24600,
                    "number_of_peaks": 9,
                    "real_peaks": 1,
                    "effective_number": 1.8,
                    "peak_accuracy": 0.10967886414488293,
                    "distance_accuracy": 3.865698597638951
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 5,
                    "real_peaks": 3,
                    "effective_number": 1,
                    "peak_accuracy": 2.9114535078900188,
                    "distance_accuracy": 5.15376493373441
                }
            ],
            "n2": [
                {
                    "NFE": 10600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                },
                {
                    "NFE": 12200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.72
                },
                {
                    "NFE": 9750,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                }
            ],
            "n3": [
                {
                    "NFE": 10000,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.104
                },
                {
                    "NFE": 11400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 20200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.032
                }
            ],
            "n4": [
                {
                    "NFE": 10600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                },
                {
                    "NFE": 15800,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.0208
                },
                {
                    "NFE": 26200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.008
                }
            ],
            "n5": [
                {
                    "NFE": 9200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.00416
                },
                {
                    "NFE": 17800,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.00448
                },
                {
                    "NFE": 23400,
                    "number_of_peaks": 26,
                    "real_peaks": 26,
                    "effective_number": 0.00832
                },
                {
                    "NFE": 12750,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 8200,
                    "number_of_peaks": 14,
                    "real_peaks": 1,
                    "effective_number": 2.8,
                    "peak_accuracy": 2.796404732613635,
                    "distance_accuracy": 2.215007711263576
                },
                {
                    "NFE": 13000,
                    "number_of_peaks": 18,
                    "real_peaks": 1,
                    "effective_number": 3.6,
                    "peak_accuracy": 2.96741826195364,
                    "distance_accuracy": 3.2344276874734836
                },
                {
                    "NFE": 21800,
                    "number_of_peaks": 15,
                    "real_peaks": 3,
                    "effective_number": 3,
                    "peak_accuracy": 3.7961540119923223,
                    "distance_accuracy": 1.5101365138685523
                },
                {
                    "NFE": 10500,
                    "number_of_peaks": 8,
                    "real_peaks": 1,
                    "effective_number": 1.6,
                    "peak_accuracy": 3.5205963876184025,
                    "distance_accuracy": 1.3218958073893388
                }
            ],
            "n2": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.44
                },
                {
                    "NFE": 13200,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.56
                },
                {
                    "NFE": 23400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.72
                },
                {
                    "NFE": 12750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                }
            ],
            "n3": [
                {
                    "NFE": 15000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                },
                {
                    "NFE": 13800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 23000,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 16750,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.056
                }
            ],
            "n4": [
                {
                    "NFE": 11800,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 13200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.0288
                },
                {
                    "NFE": 23600,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.0352
                },
                {
                    "NFE": 14750,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                }
            ],
            "n5": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.00384
                },
                {
                    "NFE": 17600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.00512
                },
                {
                    "NFE": 20000,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.00672
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.00384
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 10000,
                    "number_of_peaks": 7,
                    "real_peaks": 1,
                    "effective_number": 1.4,
                    "peak_accuracy": 3.0787058136823564,
                    "distance_accuracy": 2.876776451489529
                },
                {
                    "NFE": 10800,
                    "number_of_peaks": 13,
                    "real_peaks": 4,
                    "effective_number": 2.6,
                    "peak_accuracy": 4.361174648684968,
                    "distance_accuracy": 4.5782147518858745
                },
                {
                    "NFE": 8200,
                    "number_of_peaks": 10,
                    "real_peaks": 3,
                    "effective_number": 2,
                    "peak_accuracy": 0.02479773317635503,
                    "distance_accuracy": 4.1165278919879835
                },
                {
                    "NFE": 10750,
                    "number_of_peaks": 5,
                    "real_peaks": 2,
                    "effective_number": 1,
                    "peak_accuracy": 2.1765569140237186,
                    "distance_accuracy": 2.302484536427539
                }
            ],
            "n2": [
                {
                    "NFE": 14400,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.28
                },
                {
                    "NFE": 14400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.44
                },
                {
                    "NFE": 19400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 11250,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.36
                }
            ],
            "n3": [
                {
                    "NFE": 9400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 12800,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 23400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.152
                },
                {
                    "NFE": 11000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.056
                }
            ],
            "n4": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                },
                {
                    "NFE": 11400,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.0336
                },
                {
                    "NFE": 21000,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 14500,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                }
            ],
            "n5": [
                {
                    "NFE": 14000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 15600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0048
                },
                {
                    "NFE": 21200,
                    "number_of_peaks": 27,
                    "real_peaks": 27,
                    "effective_number": 0.00864
                },
                {
                    "NFE": 15500,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.00192
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 10600,
                    "number_of_peaks": 5,
                    "real_peaks": 4,
                    "effective_number": 1,
                    "peak_accuracy": 1.4936151903565276,
                    "distance_accuracy": 3.9426867259644105
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 13,
                    "real_peaks": 5,
                    "effective_number": 2.6,
                    "peak_accuracy": 2.314713276422933,
                    "distance_accuracy": 2.5864639758442873
                },
                {
                    "NFE": 15800,
                    "number_of_peaks": 19,
                    "real_peaks": 3,
                    "effective_number": 3.8,
                    "peak_accuracy": 2.670842011695222,
                    "distance_accuracy": 2.2352264997703926
                },
                {
                    "NFE": 8750,
                    "number_of_peaks": 12,
                    "real_peaks": 4,
                    "effective_number": 2.4,
                    "peak_accuracy": 3.761527939186048,
                    "distance_accuracy": 1.3851808242607697
                }
            ],
            "n2": [
                {
                    "NFE": 11600,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.56
                },
                {
                    "NFE": 14400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.8
                },
                {
                    "NFE": 19750,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.16
                }
            ],
            "n3": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.112
                },
                {
                    "NFE": 11800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 18400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 16250,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.04
                }
            ],
            "n4": [
                {
                    "NFE": 10800,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.016
                },
                {
                    "NFE": 16400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 22800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.0272
                },
                {
                    "NFE": 15750,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.008
                }
            ],
            "n5": [
                {
                    "NFE": 12800,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 18000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.00512
                },
                {
                    "NFE": 25400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.00608
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00224
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 13600,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.6
                },
                {
                    "NFE": 9200,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 1.1
                },
                {
                    "NFE": 10600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.8
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.5
                }
            ],
            "n2": [
                {
                    "NFE": 10200,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.11
                },
                {
                    "NFE": 58000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.06
                },
                {
                    "NFE": 21600,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.04
                },
                {
                    "NFE": 10250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.08
                }
            ],
            "n3": [
                {
                    "NFE": 8800,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.012
                },
                {
                    "NFE": 19200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.008
                },
                {
                    "NFE": 13200,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.015
                },
                {
                    "NFE": 95750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 10000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.0012
                },
                {
                    "NFE": 68000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0006
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0008
                },
                {
                    "NFE": 37250,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0002
                }
            ],
            "n5": [
                {
                    "NFE": 12800,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00009
                },
                {
                    "NFE": 26200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 37000,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.00004
                },
                {
                    "NFE": 69000,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.00004
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.8888888888888888
                },
                {
                    "NFE": 18000,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 2
                },
                {
                    "NFE": 17600,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 2.2222222222222223
                },
                {
                    "NFE": 10500,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 1.2222222222222223
                }
            ],
            "n2": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.5185185185185185
                },
                {
                    "NFE": 17600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.5925925925925926
                },
                {
                    "NFE": 27400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.6666666666666666
                },
                {
                    "NFE": 14500,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.37037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 13600,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.08641975308641975
                },
                {
                    "NFE": 24600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.09876543209876543
                },
                {
                    "NFE": 37200,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.25925925925925924
                },
                {
                    "NFE": 20750,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.08641975308641975
                }
            ],
            "n4": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0411522633744856
                },
                {
                    "NFE": 19600,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0823045267489712
                },
                {
                    "NFE": 46600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.06995884773662552
                },
                {
                    "NFE": 17500,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.03292181069958848
                }
            ],
            "n5": [
                {
                    "NFE": 14800,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 17800,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.02194787379972565
                },
                {
                    "NFE": 33400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.02606310013717421
                },
                {
                    "NFE": 18750,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.009602194787379973
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 11800,
                    "number_of_peaks": 8,
                    "real_peaks": 2,
                    "effective_number": 1.6,
                    "peak_accuracy": 4.829719839768311,
                    "distance_accuracy": 3.286885159258998
                },
                {
                    "NFE": 9600,
                    "number_of_peaks": 13,
                    "real_peaks": 3,
                    "effective_number": 2.6,
                    "peak_accuracy": 4.984683180449911,
                    "distance_accuracy": 4.190856349830202
                },
                {
                    "NFE": 15400,
                    "number_of_peaks": 7,
                    "real_peaks": 3,
                    "effective_number": 1.4,
                    "peak_accuracy": 3.00166055104479,
                    "distance_accuracy": 5.25529109180671
                },
                {
                    "NFE": 12500,
                    "number_of_peaks": 3,
                    "real_peaks": 2,
                    "effective_number": 0.6,
                    "peak_accuracy": 3.0013034007160675,
                    "distance_accuracy": 2.757062425140461
                }
            ],
            "n2": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                },
                {
                    "NFE": 15400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.48
                },
                {
                    "NFE": 15600,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.72
                },
                {
                    "NFE": 12750,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.2
                }
            ],
            "n3": [
                {
                    "NFE": 9400,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.104
                },
                {
                    "NFE": 11600,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 23600,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 13750,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.08
                }
            ],
            "n4": [
                {
                    "NFE": 14000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.0112
                },
                {
                    "NFE": 13600,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.0288
                },
                {
                    "NFE": 18800,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.0368
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.0112
                }
            ],
            "n5": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 16600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 25000,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.00672
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.00416
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 11600,
                    "number_of_peaks": 6,
                    "real_peaks": 1,
                    "effective_number": 1.2,
                    "peak_accuracy": 2.613800319074735,
                    "distance_accuracy": 2.1437029472467586
                },
                {
                    "NFE": 9200,
                    "number_of_peaks": 17,
                    "real_peaks": 3,
                    "effective_number": 3.4,
                    "peak_accuracy": 3.1974659934644425,
                    "distance_accuracy": 1.9375490387779117
                },
                {
                    "NFE": 18400,
                    "number_of_peaks": 15,
                    "real_peaks": 2,
                    "effective_number": 3,
                    "peak_accuracy": 2.707364348836327,
                    "distance_accuracy": 2.5050660730036025
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 10,
                    "real_peaks": 4,
                    "effective_number": 2,
                    "peak_accuracy": 3.846913970867418,
                    "distance_accuracy": 1.5597393340841632
                }
            ],
            "n2": [
                {
                    "NFE": 9600,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.56
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.6
                },
                {
                    "NFE": 12600,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.92
                },
                {
                    "NFE": 10750,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.36
                }
            ],
            "n3": [
                {
                    "NFE": 11600,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.088
                },
                {
                    "NFE": 12600,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.168
                },
                {
                    "NFE": 24400,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.168
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                }
            ],
            "n4": [
                {
                    "NFE": 9200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 15800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                },
                {
                    "NFE": 29600,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.0304
                },
                {
                    "NFE": 11500,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.016
                }
            ],
            "n5": [
                {
                    "NFE": 14400,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 15400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 23000,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.00736
                },
                {
                    "NFE": 19250,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.00224
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 8,
                    "real_peaks": 2,
                    "effective_number": 1.6,
                    "peak_accuracy": 0.6349819898280556,
                    "distance_accuracy": 2.500867314046795
                },
                {
                    "NFE": 8800,
                    "number_of_peaks": 15,
                    "real_peaks": 4,
                    "effective_number": 3,
                    "peak_accuracy": 0.4741994393401173,
                    "distance_accuracy": 4.116190238004219
                },
                {
                    "NFE": 17000,
                    "number_of_peaks": 10,
                    "real_peaks": 3,
                    "effective_number": 2,
                    "peak_accuracy": 1.9562338967491149,
                    "distance_accuracy": 4.717254232461118
                },
                {
                    "NFE": 10750,
                    "number_of_peaks": 7,
                    "real_peaks": 1,
                    "effective_number": 1.4,
                    "peak_accuracy": 2.7669281002258157,
                    "distance_accuracy": 2.7093997673674455
                }
            ],
            "n2": [
                {
                    "NFE": 8600,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                },
                {
                    "NFE": 10000,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 20200,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.76
                },
                {
                    "NFE": 12250,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.36
                }
            ],
            "n3": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.12
                },
                {
                    "NFE": 25000,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 12500,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.088
                }
            ],
            "n4": [
                {
                    "NFE": 11200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.0208
                },
                {
                    "NFE": 12600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.024
                },
                {
                    "NFE": 18200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.0288
                },
                {
                    "NFE": 17750,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.008
                }
            ],
            "n5": [
                {
                    "NFE": 14600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00256
                },
                {
                    "NFE": 13200,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0048
                },
                {
                    "NFE": 17200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.00544
                },
                {
                    "NFE": 17000,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 9200,
                    "number_of_peaks": 9,
                    "real_peaks": 3,
                    "effective_number": 1.8,
                    "peak_accuracy": 3.5471713323766623,
                    "distance_accuracy": 1.3278062227802947
                },
                {
                    "NFE": 12600,
                    "number_of_peaks": 7,
                    "real_peaks": 3,
                    "effective_number": 1.4,
                    "peak_accuracy": 3.3608782594385262,
                    "distance_accuracy": 2.196085536197562
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 12,
                    "real_peaks": 4,
                    "effective_number": 2.4,
                    "peak_accuracy": 1.9474684090497043,
                    "distance_accuracy": 3.566340650190169
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 5,
                    "real_peaks": 2,
                    "effective_number": 1,
                    "peak_accuracy": 2.572933931127274,
                    "distance_accuracy": 2.1125461045289917
                }
            ],
            "n2": [
                {
                    "NFE": 14400,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.32
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                },
                {
                    "NFE": 23400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.36
                }
            ],
            "n3": [
                {
                    "NFE": 13000,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.088
                },
                {
                    "NFE": 15800,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 20400,
                    "number_of_peaks": 27,
                    "real_peaks": 27,
                    "effective_number": 0.216
                },
                {
                    "NFE": 12750,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.08
                }
            ],
            "n4": [
                {
                    "NFE": 12800,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.0224
                },
                {
                    "NFE": 18400,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.0224
                },
                {
                    "NFE": 25400,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.0368
                },
                {
                    "NFE": 14750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                }
            ],
            "n5": [
                {
                    "NFE": 9800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0048
                },
                {
                    "NFE": 17200,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.00448
                },
                {
                    "NFE": 18400,
                    "number_of_peaks": 24,
                    "real_peaks": 24,
                    "effective_number": 0.00768
                },
                {
                    "NFE": 19500,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.00128
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 8600,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 1.2
                },
                {
                    "NFE": 7800,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 1.8
                },
                {
                    "NFE": 12400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 1.5
                },
                {
                    "NFE": 10250,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 1.2
                }
            ],
            "n2": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.1
                },
                {
                    "NFE": 13600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.15
                },
                {
                    "NFE": 17800,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.22
                },
                {
                    "NFE": 14500,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.005
                },
                {
                    "NFE": 14200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.008
                },
                {
                    "NFE": 32000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.002
                },
                {
                    "NFE": 73000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 11000,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.0007
                },
                {
                    "NFE": 10400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.002
                },
                {
                    "NFE": 52400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.0012
                },
                {
                    "NFE": 30000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 24600,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.00004
                },
                {
                    "NFE": 14800,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.00012
                },
                {
                    "NFE": 38600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00008
                },
                {
                    "NFE": 40250,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00002
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 12800,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.8888888888888888
                },
                {
                    "NFE": 15600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 1.7777777777777777
                },
                {
                    "NFE": 15600,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 2.3333333333333335
                },
                {
                    "NFE": 10500,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.7777777777777778
                }
            ],
            "n2": [
                {
                    "NFE": 12600,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.4444444444444444
                },
                {
                    "NFE": 18000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.5555555555555556
                },
                {
                    "NFE": 23000,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.7037037037037037
                },
                {
                    "NFE": 19250,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.2222222222222222
                }
            ],
            "n3": [
                {
                    "NFE": 13600,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.13580246913580246
                },
                {
                    "NFE": 23600,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.18518518518518517
                },
                {
                    "NFE": 17400,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.25925925925925924
                },
                {
                    "NFE": 18500,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.07407407407407407
                }
            ],
            "n4": [
                {
                    "NFE": 18800,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.0205761316872428
                },
                {
                    "NFE": 20400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.06995884773662552
                },
                {
                    "NFE": 33600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.06995884773662552
                },
                {
                    "NFE": 17250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.03292181069958848
                }
            ],
            "n5": [
                {
                    "NFE": 15200,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.009602194787379973
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0205761316872428
                },
                {
                    "NFE": 27400,
                    "number_of_peaks": 25,
                    "real_peaks": 25,
                    "effective_number": 0.03429355281207133
                },
                {
                    "NFE": 19250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.010973936899862825
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 13400,
                    "number_of_peaks": 7,
                    "real_peaks": 1,
                    "effective_number": 1.4,
                    "peak_accuracy": 0.18833827441004913,
                    "distance_accuracy": 4.471456740339343
                },
                {
                    "NFE": 14400,
                    "number_of_peaks": 9,
                    "real_peaks": 3,
                    "effective_number": 1.8,
                    "peak_accuracy": 4.979398182451929,
                    "distance_accuracy": 3.4066187286105256
                },
                {
                    "NFE": 14400,
                    "number_of_peaks": 13,
                    "real_peaks": 3,
                    "effective_number": 2.6,
                    "peak_accuracy": 0.7284931093480138,
                    "distance_accuracy": 4.516034419506691
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 8,
                    "real_peaks": 3,
                    "effective_number": 1.6,
                    "peak_accuracy": 0.0785714548862031,
                    "distance_accuracy": 3.9008479438878156
                }
            ],
            "n2": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.6
                },
                {
                    "NFE": 20800,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.56
                },
                {
                    "NFE": 11000,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                }
            ],
            "n3": [
                {
                    "NFE": 13000,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.072
                },
                {
                    "NFE": 10800,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.168
                },
                {
                    "NFE": 24200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 12500,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                }
            ],
            "n4": [
                {
                    "NFE": 14400,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.008
                },
                {
                    "NFE": 11200,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.0368
                },
                {
                    "NFE": 28800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 12750,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.016
                }
            ],
            "n5": [
                {
                    "NFE": 12200,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00256
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.00672
                },
                {
                    "NFE": 24600,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.00576
                },
                {
                    "NFE": 13250,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.00192
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 8200,
                    "number_of_peaks": 13,
                    "real_peaks": 2,
                    "effective_number": 2.6,
                    "peak_accuracy": 2.7405248302018355,
                    "distance_accuracy": 2.496287386317967
                },
                {
                    "NFE": 8800,
                    "number_of_peaks": 10,
                    "real_peaks": 2,
                    "effective_number": 2,
                    "peak_accuracy": 2.810820582854811,
                    "distance_accuracy": 1.9814254254299457
                },
                {
                    "NFE": 13800,
                    "number_of_peaks": 17,
                    "real_peaks": 5,
                    "effective_number": 3.4,
                    "peak_accuracy": 3.5844228141930246,
                    "distance_accuracy": 1.4622538277040393
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 13,
                    "real_peaks": 3,
                    "effective_number": 2.6,
                    "peak_accuracy": 3.8501563070744487,
                    "distance_accuracy": 1.5870695282134806
                }
            ],
            "n2": [
                {
                    "NFE": 15200,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.24
                },
                {
                    "NFE": 16200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                },
                {
                    "NFE": 20600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.68
                },
                {
                    "NFE": 15750,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.16
                }
            ],
            "n3": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.088
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 19600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.136
                },
                {
                    "NFE": 16500,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.048
                }
            ],
            "n4": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0096
                },
                {
                    "NFE": 16800,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.0256
                },
                {
                    "NFE": 18200,
                    "number_of_peaks": 25,
                    "real_peaks": 25,
                    "effective_number": 0.04
                },
                {
                    "NFE": 16250,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0096
                }
            ],
            "n5": [
                {
                    "NFE": 13200,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.00288
                },
                {
                    "NFE": 14800,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.00576
                },
                {
                    "NFE": 23400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.00608
                },
                {
                    "NFE": 13500,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 9200,
                    "number_of_peaks": 14,
                    "real_peaks": 4,
                    "effective_number": 2.8,
                    "peak_accuracy": 1.7582856463609593,
                    "distance_accuracy": 2.4429492631420087
                },
                {
                    "NFE": 8400,
                    "number_of_peaks": 12,
                    "real_peaks": 3,
                    "effective_number": 2.4,
                    "peak_accuracy": 0.0958012966113444,
                    "distance_accuracy": 4.090272027835169
                },
                {
                    "NFE": 9800,
                    "number_of_peaks": 21,
                    "real_peaks": 2,
                    "effective_number": 4.2,
                    "peak_accuracy": 0.8062065824993354,
                    "distance_accuracy": 3.9698591044646183
                },
                {
                    "NFE": 12000,
                    "number_of_peaks": 5,
                    "real_peaks": 1,
                    "effective_number": 1,
                    "peak_accuracy": 1.5401374999440212,
                    "distance_accuracy": 4.611555406892762
                }
            ],
            "n2": [
                {
                    "NFE": 10400,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.52
                },
                {
                    "NFE": 12200,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.64
                },
                {
                    "NFE": 17800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.8
                },
                {
                    "NFE": 9750,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.36
                }
            ],
            "n3": [
                {
                    "NFE": 9600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.128
                },
                {
                    "NFE": 11600,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 19400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.144
                },
                {
                    "NFE": 10000,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.112
                }
            ],
            "n4": [
                {
                    "NFE": 12800,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.0128
                },
                {
                    "NFE": 12400,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.0272
                },
                {
                    "NFE": 22800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.032
                },
                {
                    "NFE": 11000,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.0208
                }
            ],
            "n5": [
                {
                    "NFE": 12600,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.00352
                },
                {
                    "NFE": 10800,
                    "number_of_peaks": 23,
                    "real_peaks": 23,
                    "effective_number": 0.00736
                },
                {
                    "NFE": 22800,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.0064
                },
                {
                    "NFE": 12250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.00256
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 14000,
                    "number_of_peaks": 7,
                    "real_peaks": 5,
                    "effective_number": 1.4,
                    "peak_accuracy": 3.345875664885142,
                    "distance_accuracy": 2.1501329515411163
                },
                {
                    "NFE": 19000,
                    "number_of_peaks": 13,
                    "real_peaks": 4,
                    "effective_number": 2.6,
                    "peak_accuracy": 3.797912380628298,
                    "distance_accuracy": 1.499862606330435
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 15,
                    "real_peaks": 4,
                    "effective_number": 3,
                    "peak_accuracy": 3.2819961486601117,
                    "distance_accuracy": 2.163217732968679
                },
                {
                    "NFE": 11000,
                    "number_of_peaks": 7,
                    "real_peaks": 3,
                    "effective_number": 1.4,
                    "peak_accuracy": 1.0200760697450353,
                    "distance_accuracy": 3.7892395657786055
                }
            ],
            "n2": [
                {
                    "NFE": 9400,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 0.48
                },
                {
                    "NFE": 11200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.72
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 21,
                    "real_peaks": 21,
                    "effective_number": 0.84
                },
                {
                    "NFE": 12250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.4
                }
            ],
            "n3": [
                {
                    "NFE": 13600,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.064
                },
                {
                    "NFE": 14800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.12
                },
                {
                    "NFE": 21200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.16
                },
                {
                    "NFE": 17250,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.056
                }
            ],
            "n4": [
                {
                    "NFE": 13200,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.0176
                },
                {
                    "NFE": 14200,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.0272
                },
                {
                    "NFE": 30200,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.0304
                },
                {
                    "NFE": 14500,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.0144
                }
            ],
            "n5": [
                {
                    "NFE": 11400,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.00416
                },
                {
                    "NFE": 19000,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 0.00512
                },
                {
                    "NFE": 25400,
                    "number_of_peaks": 19,
                    "real_peaks": 19,
                    "effective_number": 0.00608
                },
                {
                    "NFE": 14250,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 9200,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 1.4
                },
                {
                    "NFE": 13600,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 1.3
                },
                {
                    "NFE": 17600,
                    "number_of_peaks": 16,
                    "real_peaks": 16,
                    "effective_number": 1.6
                },
                {
                    "NFE": 10500,
                    "number_of_peaks": 4,
                    "real_peaks": 4,
                    "effective_number": 0.4
                }
            ],
            "n2": [
                {
                    "NFE": 13400,
                    "number_of_peaks": 7,
                    "real_peaks": 7,
                    "effective_number": 0.07
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 14,
                    "real_peaks": 14,
                    "effective_number": 0.14
                },
                {
                    "NFE": 43200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.13
                },
                {
                    "NFE": 14000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.06
                }
            ],
            "n3": [
                {
                    "NFE": 12400,
                    "number_of_peaks": 3,
                    "real_peaks": 3,
                    "effective_number": 0.003
                },
                {
                    "NFE": 8400,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.018
                },
                {
                    "NFE": 12400,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 0.02
                },
                {
                    "NFE": 108750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 8400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0015
                },
                {
                    "NFE": 47600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 77200,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.0006
                },
                {
                    "NFE": 60000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 71400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 9000,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.00018
                },
                {
                    "NFE": 26000,
                    "number_of_peaks": 5,
                    "real_peaks": 5,
                    "effective_number": 0.00005
                },
                {
                    "NFE": 67000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 15000,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.8888888888888888
                },
                {
                    "NFE": 15000,
                    "number_of_peaks": 12,
                    "real_peaks": 12,
                    "effective_number": 1.3333333333333333
                },
                {
                    "NFE": 15200,
                    "number_of_peaks": 20,
                    "real_peaks": 20,
                    "effective_number": 2.2222222222222223
                },
                {
                    "NFE": 9750,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.8888888888888888
                }
            ],
            "n2": [
                {
                    "NFE": 17200,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.3333333333333333
                },
                {
                    "NFE": 15200,
                    "number_of_peaks": 18,
                    "real_peaks": 18,
                    "effective_number": 0.6666666666666666
                },
                {
                    "NFE": 27200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.48148148148148145
                },
                {
                    "NFE": 16000,
                    "number_of_peaks": 11,
                    "real_peaks": 11,
                    "effective_number": 0.4074074074074074
                }
            ],
            "n3": [
                {
                    "NFE": 15800,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 20800,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.18518518518518517
                },
                {
                    "NFE": 21600,
                    "number_of_peaks": 17,
                    "real_peaks": 17,
                    "effective_number": 0.20987654320987653
                },
                {
                    "NFE": 18250,
                    "number_of_peaks": 9,
                    "real_peaks": 9,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n4": [
                {
                    "NFE": 13200,
                    "number_of_peaks": 10,
                    "real_peaks": 10,
                    "effective_number": 0.0411522633744856
                },
                {
                    "NFE": 23800,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.053497942386831275
                },
                {
                    "NFE": 25200,
                    "number_of_peaks": 13,
                    "real_peaks": 13,
                    "effective_number": 0.053497942386831275
                },
                {
                    "NFE": 19250,
                    "number_of_peaks": 8,
                    "real_peaks": 8,
                    "effective_number": 0.03292181069958848
                }
            ],
            "n5": [
                {
                    "NFE": 16000,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.00823045267489712
                },
                {
                    "NFE": 21400,
                    "number_of_peaks": 15,
                    "real_peaks": 15,
                    "effective_number": 0.0205761316872428
                },
                {
                    "NFE": 30600,
                    "number_of_peaks": 22,
                    "real_peaks": 22,
                    "effective_number": 0.03017832647462277
                },
                {
                    "NFE": 21250,
                    "number_of_peaks": 6,
                    "real_peaks": 6,
                    "effective_number": 0.00823045267489712
                }
            ]
        }
    }
]