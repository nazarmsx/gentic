var globalStats=[
    {
        "f15": {
            "n1": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00014395498152075525,
                    "distance_accuracy": 3.0331717015222015
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0019317930173841091,
                    "distance_accuracy": 4.460969258185772
                },
                {
                    "NFE": 2600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.000008919265351181593,
                    "distance_accuracy": 3.0339825459749465
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 2.6038218248203293e-8,
                    "distance_accuracy": 3.6899090273262347
                }
            ],
            "n2": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6669466085857048,
                    "distance_accuracy": 3.784141451468464
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6673617015602813,
                    "distance_accuracy": 3.7824629172584565
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6669088471445894,
                    "distance_accuracy": 3.7841730369229745
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.667033811482377,
                    "distance_accuracy": 3.78405574586225
                }
            ],
            "n2": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004126990514716766,
                    "distance_accuracy": 4.7604416639842
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00041244542028662234,
                    "distance_accuracy": 2.6503922504321666
                },
                {
                    "NFE": 2600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00041279638610836056,
                    "distance_accuracy": 3.968661812980733
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0002977064641495053,
                    "distance_accuracy": 3.6137738837223923
                }
            ],
            "n2": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4511711787373889,
                    "distance_accuracy": 4.097843507307166
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 0,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.373585795319745,
                    "distance_accuracy": 4.011048906620262
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4521027300758107,
                    "distance_accuracy": 4.099494825523979
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4532071326308849,
                    "distance_accuracy": 4.102362140444661
                }
            ],
            "n2": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0002
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n2": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 7600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00823045267489712
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                }
            ],
            "n5": [
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0027434842249657062
                },
                {
                    "NFE": 8500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 2.9542545076921556e-8,
                    "distance_accuracy": 2.5802004981735123
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0009514968447804639,
                    "distance_accuracy": 3.687477111269928
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.8042679217344926e-7,
                    "distance_accuracy": 3.033833164339782
                },
                {
                    "NFE": 3500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0003520683542571623,
                    "distance_accuracy": 3.034485774698238
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00064
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6605916069589357,
                    "distance_accuracy": 3.768973432787112
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.667387161419187,
                    "distance_accuracy": 3.782903726479205
                },
                {
                    "NFE": 2600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6663573431799863,
                    "distance_accuracy": 3.7844194073043456
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.653574692324662,
                    "distance_accuracy": 3.7779242972071567
                }
            ],
            "n2": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0003978773604593755,
                    "distance_accuracy": 3.968891429639868
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004008112383465656,
                    "distance_accuracy": 3.9687475774699794
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0003533922200600337,
                    "distance_accuracy": 2.6505360706048533
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00040849496561190524,
                    "distance_accuracy": 3.9686818305716636
                }
            ],
            "n2": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00064
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4532224267622729,
                    "distance_accuracy": 4.102146027261279
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4522991028313639,
                    "distance_accuracy": 4.099910140392897
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.44124907845426,
                    "distance_accuracy": 4.097533761727286
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4373665238668099,
                    "distance_accuracy": 4.094869465538979
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 7200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.2222222222222222
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n2": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 9400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 11800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00823045267489712
                },
                {
                    "NFE": 9500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                }
            ],
            "n5": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0027434842249657062
                },
                {
                    "NFE": 9750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00005601552938550025,
                    "distance_accuracy": 3.6894203173732665
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00009677150474118879,
                    "distance_accuracy": 3.690442626236264
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 3.399885611932163e-7,
                    "distance_accuracy": 3.689933882778331
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 7.856881478751632e-8,
                    "distance_accuracy": 4.459060128104145
                }
            ],
            "n2": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6399386961885192,
                    "distance_accuracy": 3.740366161114726
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 0,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.551786923023696,
                    "distance_accuracy": 3.69481264893946
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6619408558094724,
                    "distance_accuracy": 3.771109452924752
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6673946954172785,
                    "distance_accuracy": 3.7823983432800246
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00026042114483615375,
                    "distance_accuracy": 4.761703296325605
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0005125042344938935,
                    "distance_accuracy": 3.966088127668457
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00033385336083979755,
                    "distance_accuracy": 4.761034147111964
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00028617474404124543,
                    "distance_accuracy": 3.2026116970071774
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4531795878691411,
                    "distance_accuracy": 4.102467958232349
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4531097819314058,
                    "distance_accuracy": 4.101621190796205
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4532154577008316,
                    "distance_accuracy": 4.102050832575139
                },
                {
                    "NFE": 3750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.453070485075315,
                    "distance_accuracy": 4.1026744903042855
                }
            ],
            "n2": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.08
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n2": [
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 8000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 9800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00823045267489712
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                }
            ],
            "n5": [
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0027434842249657062
                },
                {
                    "NFE": 8750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00016825623627758635,
                    "distance_accuracy": 4.458091404282577
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0001583960269641027,
                    "distance_accuracy": 3.033133067891336
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0000035363570677349188,
                    "distance_accuracy": 2.580147626477937
                },
                {
                    "NFE": 3750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.000004636298919225013,
                    "distance_accuracy": 3.0337422119216826
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6678978882365885,
                    "distance_accuracy": 3.7822728942941604
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.666535664820577,
                    "distance_accuracy": 3.7796241063575065
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6673842293171357,
                    "distance_accuracy": 3.7827577724111134
                },
                {
                    "NFE": 3750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6668507776121184,
                    "distance_accuracy": 3.784216304502867
                }
            ],
            "n2": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00040925125913227056,
                    "distance_accuracy": 3.9685183809265974
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00038630592041100087,
                    "distance_accuracy": 4.760074302471161
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00039944492894650185,
                    "distance_accuracy": 3.9688788563643196
                },
                {
                    "NFE": 3750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004128566069053763,
                    "distance_accuracy": 3.2019455533075534
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4511974533517344,
                    "distance_accuracy": 4.103020790376207
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4489097157762507,
                    "distance_accuracy": 4.094393631902454
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 2,
                    "real_peaks": 1,
                    "effective_number": 0.4,
                    "peak_accuracy": 1.3756959632800991,
                    "distance_accuracy": 3.8844739891322893
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4413523848628853,
                    "distance_accuracy": 4.0976027390578755
                }
            ],
            "n2": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 9000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00064
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 7600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n2": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.07407407407407407
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.07407407407407407
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 6600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 9000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00823045267489712
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                }
            ],
            "n5": [
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 6400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0027434842249657062
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0027434842249657062
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0014561114550243337,
                    "distance_accuracy": 5.298808293072106
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.5741401515612097e-8,
                    "distance_accuracy": 2.5801933197910216
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00010603475705606158,
                    "distance_accuracy": 3.386127243183383
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 8.200026058169385e-7,
                    "distance_accuracy": 5.296816291791321
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6577350618824584,
                    "distance_accuracy": 3.764654497026417
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.664693847449825,
                    "distance_accuracy": 3.7751776683810085
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6673853030808619,
                    "distance_accuracy": 3.7827887163579677
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.667370721410414,
                    "distance_accuracy": 3.782476430123906
                }
            ],
            "n2": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.016
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004113004989179103,
                    "distance_accuracy": 3.2018722372825392
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00041282287444188714,
                    "distance_accuracy": 2.6503776504906504
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004128526598133897,
                    "distance_accuracy": 2.65037521668591
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004128564603392837,
                    "distance_accuracy": 4.760416055708572
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.452994680374196,
                    "distance_accuracy": 4.102759956758925
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4414808498871847,
                    "distance_accuracy": 4.097688350281113
                },
                {
                    "NFE": 2600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4532227798790105,
                    "distance_accuracy": 4.1022102638728075
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4532228877317972,
                    "distance_accuracy": 4.1022052966367815
                }
            ],
            "n2": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.08
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00064
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 9250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 6600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 7600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n2": [
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 6400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.07407407407407407
                },
                {
                    "NFE": 8250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 6600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00823045267489712
                },
                {
                    "NFE": 8500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                }
            ],
            "n5": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 7200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 9400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0027434842249657062
                },
                {
                    "NFE": 8500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00006262640894372229,
                    "distance_accuracy": 5.296163426544224
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00004141677011015865,
                    "distance_accuracy": 5.296277692855914
                },
                {
                    "NFE": 2600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 2.6921167961546644e-8,
                    "distance_accuracy": 4.459052357173331
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0011070728862693224,
                    "distance_accuracy": 3.032454646122383
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.08
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6360415406673505,
                    "distance_accuracy": 3.7353477756159323
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 0,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6199369957425584,
                    "distance_accuracy": 3.715217587541786
                },
                {
                    "NFE": 2600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6673871294767952,
                    "distance_accuracy": 3.7828885724658923
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.667326098459057,
                    "distance_accuracy": 3.78347287427722
                }
            ],
            "n2": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.08
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.016
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 9000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00040804596540411353,
                    "distance_accuracy": 3.2018151691002186
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0003572885483714705,
                    "distance_accuracy": 2.6500769103355477
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004120540501478809,
                    "distance_accuracy": 3.2019003622648787
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004128562567687899,
                    "distance_accuracy": 3.201946591671994
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 8000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4491301639565808,
                    "distance_accuracy": 4.094710246983178
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4532228229110418,
                    "distance_accuracy": 4.102160205405011
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.4,
                    "peak_accuracy": 1.4162280171670147,
                    "distance_accuracy": 3.932442782762586
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.3354257642062644,
                    "distance_accuracy": 3.340326408976257
                }
            ],
            "n2": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00064
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 8750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 7400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.2222222222222222
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n2": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 10800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.07407407407407407
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 6600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 6600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 6400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00823045267489712
                },
                {
                    "NFE": 8750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                }
            ],
            "n5": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 6800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 9000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0027434842249657062
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.001278372318909704,
                    "distance_accuracy": 4.455996788669757
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00008905581639540028,
                    "distance_accuracy": 3.033618116292325
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 7.040733446261882e-7,
                    "distance_accuracy": 2.5802160120294286
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00000893215183483953,
                    "distance_accuracy": 3.0339824468498025
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6610075472321664,
                    "distance_accuracy": 3.7696237728813307
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6394814078039217,
                    "distance_accuracy": 3.7397735663237
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6666242939278966,
                    "distance_accuracy": 3.77983851772134
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6666922433453313,
                    "distance_accuracy": 3.784308502635862
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 8000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00064
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004946376583555567,
                    "distance_accuracy": 3.2030277673927325
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00041142545643824135,
                    "distance_accuracy": 3.202013698666429
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00040895373169236304,
                    "distance_accuracy": 3.9687733515709454
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004128564948217006,
                    "distance_accuracy": 5.458352406758632
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.452937885915293,
                    "distance_accuracy": 4.102810285276975
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4532071515742166,
                    "distance_accuracy": 4.101988516833033
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4532209165086418,
                    "distance_accuracy": 4.102114264338879
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4769584013812493,
                    "distance_accuracy": 3.957294165621949
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 6400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 8000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 6400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 8200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0002
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 8500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 8200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00002
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 7400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 8000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 8000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 7200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 6600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 8250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                }
            ],
            "n5": [
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 8250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00001995128399323587,
                    "distance_accuracy": 4.459328021107675
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.000016057900727783903,
                    "distance_accuracy": 3.689654672144587
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 4.073018589068056e-9,
                    "distance_accuracy": 2.5801948213971238
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 2.104982854689297e-12,
                    "distance_accuracy": 2.5801964036903833
                }
            ],
            "n2": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6617851033454278,
                    "distance_accuracy": 3.770858787829518
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6673569055641613,
                    "distance_accuracy": 3.783317438005714
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6673870768866683,
                    "distance_accuracy": 3.7829248593491793
                },
                {
                    "NFE": 3500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.667354406211318,
                    "distance_accuracy": 3.7824022439799605
                }
            ],
            "n2": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00022876739990218553,
                    "distance_accuracy": 3.967347908326695
                },
                {
                    "NFE": 2600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00040768712305905286,
                    "distance_accuracy": 3.968794889081
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0003750917018614519,
                    "distance_accuracy": 3.201565803656556
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00041285655531331233,
                    "distance_accuracy": 2.6503727937648667
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4358900061372033,
                    "distance_accuracy": 4.077921986421663
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.447507633452933,
                    "distance_accuracy": 4.092436023816379
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4415566726921556,
                    "distance_accuracy": 4.097738793076947
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.453005728698456,
                    "distance_accuracy": 4.102749006265377
                }
            ],
            "n2": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.016
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 9250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n2": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 11600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.07407407407407407
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00823045267489712
                },
                {
                    "NFE": 9750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                }
            ],
            "n5": [
                {
                    "NFE": 7600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 9200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0027434842249657062
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00005205523502249676,
                    "distance_accuracy": 4.458534757266088
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 3.862689501588079e-8,
                    "distance_accuracy": 3.0338569093607686
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00023280120258006853,
                    "distance_accuracy": 3.386566188979056
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 9.303673437210946e-7,
                    "distance_accuracy": 3.6898416166580086
                }
            ],
            "n2": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 8500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6664768697465902,
                    "distance_accuracy": 3.7843899760428
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6657635464551448,
                    "distance_accuracy": 3.7778992182427396
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6667972817871177,
                    "distance_accuracy": 3.7842512495293095
                },
                {
                    "NFE": 4750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6673871539829017,
                    "distance_accuracy": 3.782902754957655
                }
            ],
            "n2": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00064
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00041237745454258956,
                    "distance_accuracy": 3.9685996986816705
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00035827308908042976,
                    "distance_accuracy": 5.457886591195128
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00041191218185387957,
                    "distance_accuracy": 4.759227433413063
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00040337462594453033,
                    "distance_accuracy": 3.614369492976439
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.08
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.3354247248609399,
                    "distance_accuracy": 3.340847819115184
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.450934661821418,
                    "distance_accuracy": 4.097448083739172
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4063952385160676,
                    "distance_accuracy": 4.044530598014859
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4355430452741196,
                    "distance_accuracy": 4.09357964229134
                }
            ],
            "n2": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.08
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.016
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 9250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 9000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n2": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 7200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 7250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                }
            ],
            "n5": [
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 7200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0027434842249657062
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                }
            ]
        }
    },
    {
        "f15": {
            "n1": [
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0016884753746054804,
                    "distance_accuracy": 5.298912584908758
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.000009005997819167,
                    "distance_accuracy": 2.580115481678522
                },
                {
                    "NFE": 2800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00037983442843914084,
                    "distance_accuracy": 2.5799801395089754
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0000031777163954638254,
                    "distance_accuracy": 3.0337608720881843
                }
            ],
            "n2": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f16": {
            "n1": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6403112723716968,
                    "distance_accuracy": 3.7679833166709575
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6673847462013618,
                    "distance_accuracy": 3.7827297144677696
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6673857909533178,
                    "distance_accuracy": 3.78274101878234
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.6672808770364926,
                    "distance_accuracy": 3.7819412939929182
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f18": {
            "n1": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0006360910838728584,
                    "distance_accuracy": 2.6485655492239526
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0004095838013005171,
                    "distance_accuracy": 4.760301561799195
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.00041108462984140726,
                    "distance_accuracy": 5.458285758197461
                },
                {
                    "NFE": 3750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 0.0003992766211701193,
                    "distance_accuracy": 4.760178514487718
                }
            ],
            "n2": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 3400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f19": {
            "n1": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.4532216958994337,
                    "distance_accuracy": 4.102128453397826
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.335262975403729,
                    "distance_accuracy": 3.3394650405781228
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.453223087970203,
                    "distance_accuracy": 4.102183588351567
                },
                {
                    "NFE": 4250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.2,
                    "peak_accuracy": 1.444765758646402,
                    "distance_accuracy": 4.088804455102875
                }
            ],
            "n2": [
                {
                    "NFE": 3800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                },
                {
                    "NFE": 6500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.04
                }
            ],
            "n3": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.008
                }
            ],
            "n4": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0032
                },
                {
                    "NFE": 8500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0016
                }
            ],
            "n5": [
                {
                    "NFE": 5600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00032
                }
            ]
        },
        "f20": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 4800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.2
                },
                {
                    "NFE": 3000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1
                }
            ],
            "n2": [
                {
                    "NFE": 4200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                },
                {
                    "NFE": 6250,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.01
                }
            ],
            "n3": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 4600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.001
                }
            ],
            "n4": [
                {
                    "NFE": 5200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                },
                {
                    "NFE": 6750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0001
                }
            ],
            "n5": [
                {
                    "NFE": 7000,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00002
                },
                {
                    "NFE": 4400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                },
                {
                    "NFE": 4500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00001
                }
            ]
        },
        "f24": {
            "n1": [
                {
                    "NFE": 3600,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 3200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                },
                {
                    "NFE": 6800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.2222222222222222
                },
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.1111111111111111
                }
            ],
            "n2": [
                {
                    "NFE": 4000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 4200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.07407407407407407
                },
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                },
                {
                    "NFE": 7500,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.037037037037037035
                }
            ],
            "n3": [
                {
                    "NFE": 5400,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                },
                {
                    "NFE": 8400,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 7200,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.024691358024691357
                },
                {
                    "NFE": 8750,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.012345679012345678
                }
            ],
            "n4": [
                {
                    "NFE": 5000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                },
                {
                    "NFE": 9800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00823045267489712
                },
                {
                    "NFE": 6800,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.00823045267489712
                },
                {
                    "NFE": 7000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.00411522633744856
                }
            ],
            "n5": [
                {
                    "NFE": 5800,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 6200,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                },
                {
                    "NFE": 9600,
                    "number_of_peaks": 2,
                    "real_peaks": 2,
                    "effective_number": 0.0027434842249657062
                },
                {
                    "NFE": 6000,
                    "number_of_peaks": 1,
                    "real_peaks": 1,
                    "effective_number": 0.0013717421124828531
                }
            ]
        }
    }
]