String.prototype.toBits=function(){
    var bits=[];

    for(var i=0;i<this.length;i+=8){

        bits.push(parseInt(this.substr(i,8),2));
    }
    return bits;
};
window. utils={};
utils.populateItem=populateItem;
utils.deserializeItem=deserializeItem;



//console.log(populateItem(2,0,1))
function populateItem(len, min, max) {
    function getRandomArbitrary(min, max) {
        return Math.random() * (max - min) + min;
    }
    var temp=new Array(len);

    for (var i = 0; i < len; ++i) {

        temp[i]=encode(getRandomArbitrary(min,max));}
    return temp.join("");
}
function serializeItem(array){

    var temp=new Array(array.length);
    var offset = 0;

    for (var i = 0; i < array.length; ++i) {
        temp[i]=encode(array[i])
    }
    return temp.join("");
}

function deserializeItem(binary_string){
    var res = new Float32Array(binary_string.length / 32);
    var offset = 0;

    for (var i = 0; i < res.length; ++i) {
        res[i] = decode(binary_string.substr(offset,32));
        offset += 32;

    }
    return res;
}



function f15(x) {
    var args = [...arguments];
    return (1 / args.length) * args.reduce((prev, xi)=>prev + Math.pow(Math.sin(5 * Math.PI * xi), 6), 0);
}

//console.log(f16(0.9));

function f16(x) {
    var args = [...arguments];
    return args.reduce((prev, xi)=>prev + (Math.pow(Math.E, -2 * (Math.LN2) * Math.pow(((xi - 0.1) / 0.8), 2))) * Math.pow(Math.sin(5 * Math.PI * xi), 6), 0);
}
//console.log(f18(0.15));

function f18(x) {
    var args = [...arguments];
    return (1 / args.length) * args.reduce((prev, xi)=>prev + Math.pow(Math.sin(5 * Math.PI * (Math.pow(xi, 0.75) - 0.05)), 6), 0);
}
//console.log(f19(0.247));

function f19(x) {
    var args = [...arguments];
    return args.reduce((prev, xi)=>prev + (Math.pow(Math.E, -2 * (Math.LN2) * Math.pow(((xi - 0.1) / 0.854), 2))) * Math.pow(Math.sin(5 * Math.PI * (Math.pow(xi, 0.75) - 0.05)), 6), 0);
}

function f20(x) { // -5.12 5.12
    const A = 10;
    const w = 2 * Math.PI;
    var args = [...arguments];
    //return args.length*A+args.reduce((prev,xi)=>prev+xi*xi-A*Math.cos(w*xi),0);
    return args.reduce((prev, xi)=>prev + (10 * Math.cos(2 * Math.PI * xi) - xi * xi), 0) - 10 * args.length

}

function f22(x) { // -600 - 600
    const A = 10;
    const w = 2 * Math.PI;
    var args = [...arguments];
    return (10 - (args.reduce((prev, xi)=>prev + (xi * xi) / 4000.0, 0) - args.reduce((prev, xi)=>prev * (Math.cos(xi / Math.sqrt(1))), 1) + 1));
}

function f24(x) { // -10 - 10
    var args = [...arguments];
    return -1 * args.reduce((prev, xi)=> {
            var sum = 0;
            for (var j = 1; j <= 5; ++j) {
                sum += j * Math.cos((j + 1) * xi + j)
            }
            return prev * sum;

        }, 1);
}
function f46(x) { // -3 - 3
    var args = [...arguments];
    var x1 = args[0];
    var x2 = args[1];

    return -1 * ((4 - 2.1 * x1 * x1 + Math.pow(x1, 4) / 3) * x1 * x1 + x1 * x2 + 4 * (x2 * x2 - 1) * x2 * x2)
}
function generateInitialPopulation(length, dimension, min, max) {

    var res = [];

    for (var i = 0; i < length; ++i) {
        res.push(populateItem(dimension, min, max));
    }
    return res;
}

function getMaxCount(population, options) {
    //console.log(options);
    var eps = 0.001;
    var bestIndex = 0;
    var processed = {};
    var unprocessed = population.reduce((prev, el, index)=> {
        prev[index] = index;
        return prev;
    }, {});
    var seeds = [];
    while (Object.keys(processed).length < population.length) {
        var best = null;
        var found = false;
        for (var index in unprocessed) {

            if (!best) {
                best = unprocessed[index];
            }
            else if (Math.abs(population[unprocessed[index]].fitness - population[best].fitness) <= eps) {
                best = unprocessed[index];
            }
        }

        for (var i = 0; i < seeds.length; ++i) {
            if ((population[seeds[i]].fitness - population[best].fitness) <= eps) {
                found = true;
            }
        }
        if (!found)
            seeds.push(best);

        processed[best] = true;
        delete unprocessed[best];

        //console.log(Object.keys(processed).length);
    }
    //console.log(seeds);\

    var total = 0;

    if (options.peaks && population[0].x.length == 1) {
        var realPeaks = options.peaks.reduce((p, c)=> {
            p[c] = 0;
            return p;
        }, {});
        for (i = 0; i < seeds.length; ++i) {
            //console.log(population[seeds[i]].x[0]);
            for (var k = 0; k < options.peaks.length; ++k) {
                if (Math.abs(population[seeds[i]].x[0] - options.peaks[k]) < 0.004) {
                    realPeaks[options.peaks[k]] += 1;
                }

            }

        }
        for (var prop in realPeaks) {
            total += realPeaks[prop];
        }
    }
    else {
        total = seeds.length;
    }


    var res = {
        number_of_peaks: seeds.length,
        real_peaks: total
    };


    return res;
}


var f15_config = {
    min: 0,
    max: 1,
    func: f15,
    name: "",
    peakCount: n=>Math.pow(5, n),
    peaks: [0.1, 0.3, 0.5, 0.7, 0.9]
};
var f16_config = {min: 0, max: 1, func: f16, peakCount: n=>Math.pow(5, n), peaks: [0.1, 0.3, 0.5, 0.7, 0.9]};
var f18_config = {
    min: 0,
    max: 1,
    func: f18,
    peakCount: n=>Math.pow(5, n),
    peaks: [0.08, 0.247, 0.451, 0.681, 0.934]
};
var f19_config = {
    min: 0,
    max: 1,
    func: f19,
    peakCount: n=>Math.pow(5, n),
    peaks: [0.08, 0.247, 0.451, 0.681, 0.934]
};
var f20_config = {min: -5.12, max: 5.12, func: f20, peakCount: n=>Math.pow(10, n)};
var f22_config = {min: -100, max: 100, func: f22};
var f46_config = {min: -3, max: 3, min_y: -2, max_y: 2, func: f46};
var f24_config = {min: -10, max: 10, func: f24, peakCount: n=>3 * Math.pow(3, n)};


var  testFunctions = [
    f15_config,
     f16_config,
    f18_config,
    f19_config,
    f20_config,
    f24_config

];

var  testFunctionsMapper ={
    f15:f15_config,
    f16: f16_config,
    f18:f18_config,
   f19: f19_config,
    f20:f20_config,
    f22:f22_config,

    f24:f24_config

};


function packIEEE754(v, ebits, fbits) {

    var bias = (1 << (ebits - 1)) - 1,
        s, e, f, ln,
        i, bits, str, bytes;

    // Compute sign, exponent, fraction
    if (v !== v) {
        // NaN
        // http://dev.w3.org/2006/webapi/WebIDL/#es-type-mapping
        e = (1 << bias) - 1; f = Math.pow(2, fbits - 1); s = 0;
    } else if (v === Infinity || v === -Infinity) {
        e = (1 << bias) - 1; f = 0; s = (v < 0) ? 1 : 0;
    } else if (v === 0) {
        e = 0; f = 0; s = (1 / v === -Infinity) ? 1 : 0;
    } else {
        s = v < 0;
        v = Math.abs(v);

        if (v >= Math.pow(2, 1 - bias)) {
            // Normalized
            ln = Math.min(Math.floor(Math.log(v) / Math.LN2), bias);
            e = ln + bias;
            f = Math.round(v * Math.pow(2, fbits - ln) - Math.pow(2, fbits));
        } else {
            // Denormalized
            e = 0;
            f = Math.round(v / Math.pow(2, 1 - bias - fbits));
        }
    }

    // Pack sign, exponent, fraction
    bits = [];
    for (i = fbits; i; i -= 1) { bits.push(f % 2 ? 1 : 0); f = Math.floor(f / 2); }
    for (i = ebits; i; i -= 1) { bits.push(e % 2 ? 1 : 0); e = Math.floor(e / 2); }
    bits.push(s ? 1 : 0);
    bits.reverse();
    str = bits.join('');

    // Bits to bytes
    bytes = [];
    while (str.length) {
        bytes.push(parseInt(str.substring(0, 8), 2));
        str = str.substring(8);
    }
    return bytes;
}

function unpackIEEE754(bytes, ebits, fbits) {

    // Bytes to bits
    var bits = [], i, j, b, str,
        bias, s, e, f;

    for (i = bytes.length; i; i -= 1) {
        b = bytes[i - 1];
        for (j = 8; j; j -= 1) {
            bits.push(b % 2 ? 1 : 0); b = b >> 1;
        }
    }
    bits.reverse();
    str = bits.join('');

    // Unpack sign, exponent, fraction
    bias = (1 << (ebits - 1)) - 1;
    s = parseInt(str.substring(0, 1), 2) ? -1 : 1;
    e = parseInt(str.substring(1, 1 + ebits), 2);
    f = parseInt(str.substring(1 + ebits), 2);

    // Produce number
    if (e === (1 << ebits) - 1) {
        return f !== 0 ? NaN : s * Infinity;
    } else if (e > 0) {
        // Normalized
        return s * Math.pow(2, e - bias) * (1 + f / Math.pow(2, fbits));
    } else if (f !== 0) {
        // Denormalized
        return s * Math.pow(2, -(bias - 1)) * (f / Math.pow(2, fbits));
    } else {
        return s < 0 ? -0 : 0;
    }
}




function encode(n){

    return packIEEE754(n, 8, 23)
        .reduce((pr,e)=>{
        pr+=getZeros(e.toString(2));
        return pr;
    },"")
}

function decode(n){
    return unpackIEEE754(n.toBits(), 8, 23);
}
function getZeros(str){
    var count=8-str.length,
        res="";
    for(var i=0;i<count;++i){

        res+="0";
    }
    return res+str;
}