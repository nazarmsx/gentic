

function getPoints(func,start,end){
    var delta=0.005;
    var x=[];
    var y=[];

    for(var i=start;i<end+delta;i+=delta){
        x.push(i);
        y.push(func(i));
    }

    return {x:x,y:y}
}

function getPoints3D(func,start,end){
    var delta=0.015;
    var x=[];
    var y=[];
    var z=[];

    for(var i=start;i<end+delta;i+=delta){
        for(var j=start;j<end+delta;j+=delta){
            x.push(i);
            y.push(j)
            z.push(func(i,j));

        }

    }
    return {x:x,y:y,z:z}

}



//console.log(getPoints3D(utils.f15,0,1));

