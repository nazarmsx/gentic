var Genetic = require("genetic-js");
var assert = require("assert");
var BitArray = require('node-bitarray');
var genetic = Genetic.create();

genetic.optimize = Genetic.Optimize.Maximize;
genetic.select1 = Genetic.Select1.Tournament2;
genetic.select2 = Genetic.Select2.Tournament2;


genetic.seed = function () {
    //console.log(this.userData.nextIndex);
    if(this.userData.initialPopulation){
     return this.userData.initialPopulation[this.userData.nextIndex++];
    }
    return this.userData.utils.populateItem(this.userData.dimension,this.userData.config.min,this.userData.config.max);
};
String.prototype.replaceAt = function (index, character) {
    return this.substr(0, index) + character + this.substr(index + character.length);
};
genetic.mutate = function (entity) {
    function getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    var index = getRandomInt(0, entity.length - 1);

    var t = entity.replaceAt(index, entity.charAt(index)=='1' ? '0' : '1') ;
   //return t;
    return entity;
};

genetic.crossover = function (mother, father) {

    var len = mother.length;
    var ca = Math.floor(Math.random() * len);
    var cb = Math.floor(Math.random() * len);
    if (ca > cb) {
        var tmp = cb;
        cb = ca;
        ca = tmp;
    }

    var son = father.substr(0, ca) + mother.substr(ca, cb - ca) + father.substr(cb);
    var daughter = mother.substr(0, ca) + father.substr(ca, cb - ca) + mother.substr(cb);

    return [son, daughter];
};


genetic.fitness = function (entity) {
    var fitness = 0;
    var x_params = this.userData.utils.deserializeItem(entity);
    fitness = this.userData.config.func.apply(null, x_params);
    for(var i=0;i<x_params.length;++i){
        if(x_params[i]<this.userData.config.min ||x_params[i]>this.userData.config.max){
          return 0;
        }
    }
    return fitness;
};

genetic.generation = function (pop, generation, stats) {
    // stop running once we've reached the solution
    //console.log(stats.mean);
    if (Math.abs(this.userData.previous_mean - stats.mean) < 0.0001) {
        this.userData.mean_counter++;
    }
    else {
        this.userData.mean_counter = 0;
    }


    this.userData.previous_mean = stats.mean;

    if (this.userData.redis) {


        var data = {x: [], y: [],z:[]};

        for (var i = 0; i < pop.length; ++i) {
            data.x.push(this.userData.utils.deserializeItem(pop[i].entity)[0]);
            data.y.push(pop[i].fitness)
            //console.log(this.userData)
            if(this.userData.dimension==2){
                data.z.push(this.userData.utils.deserializeItem(pop[i].entity)[1]);
            };
            pop[i].x = this.userData.utils.deserializeItem(pop[i].entity);
        }

        if(this.userData.redis ){
            console.socket.emit("generation", JSON.stringify(data));

        }
    }
    //console.log('Generation № '+generation);
    //console.log(stats.mean);

    if (this.userData.mean_counter > 5) {
        if(this.userData.redis){
            console.socket.emit("generation", JSON.stringify({final: true}));
        }
        if(this.userData.callback){
            var data = {x: [], y: []};

            for (var i = 0; i < pop.length; ++i) {
                data.x.push(this.userData.utils.deserializeItem(pop[i].entity)[0]);


                data.y.push(pop[i].fitness)
                pop[i].x = this.userData.utils.deserializeItem(pop[i].entity);
            }

            this.userData.callback({generation_number:generation,population:pop});
        }
    }
    return this.userData.mean_counter <= 5;
};

var config = {
    "iterations": 4000,
    "size": 200,
    "crossover": 0.8,
    "mutation": 0.1,
    "GG":0.1,
    "CF":3,
    "skip": 0,
    "strategy":1
};

//execute();
//execute();

function execute() {
    console.time('executionTime');
    var userData = {
        dimension: 1,
        previous_mean: 0,
        mean_counter: 0,
        utils: require("./test.js"),
        config:require("./test.js").f16_config,
        publish_count:0,
        redis:false,
        callback:function(data){
           // var testFunctions = require('../core/test.js');
            //console.log(userData);
            userData.utils.getMaxCount(data.population);
        }
    };
    genetic.evolve(config, userData);
    console.timeEnd('executionTime')

}
function excute2(funcName,dim){

    console.time('executionTime');
    var userData = {
        dimension: dim,
        previous_mean: 0,
        mean_counter: 0,
        utils: require("./test.js"),
        config:require("./test.js")[funcName+ "_config"],
        publish_count:0,
        redis:true,
        callback:function(data){
            console.log(userData.utils.getMaxCount(data.population,userData.config));
        }
    };
    genetic.evolve(config, userData);
    console.timeEnd('executionTime')
}


function executeWithStats(config,function_config,cb) {

    var userData = {
        dimension: config.dimension,
        size: config.size,
        previous_mean: 0,
        mean_counter: 0,
        utils: require("./test.js"),
        config:function_config,
        publish_count:0,
        redis:false,
        initialPopulation:config.initialPopulation,
        nextIndex:0
    };

    userData.callback=cb;

    genetic.evolve(config, userData);
}



var exports = module.exports = {};

exports.execute = execute;
exports.excute2 = excute2;
exports.executeWithStats = executeWithStats;

