var async = require('async'),
    solver = require('./solver'),
    testFunctions = require('./test'),
    fs=require('fs'),
     exports=module.exports={};
process.env.NODE_ENV = 'production';

//var userData = {
//    dimension: 1,
//    previous_mean: 0,
//    mean_counter: 0,
//    utils: ,
//    config:require("./test.js")[funcName+ "_config"],
//    publish_count:0,
//    redis:true
//};
var configurations = [
    {
        "iterations": 4000,
        "size": 200,
        "crossover": 0.8,
        "mutation": 0.1,
        "GG": 0.1,
        "CF": 2,
        "skip": 0,
        "strategy":1
    },
    {
        "iterations": 4000,
        "size": 200,
        "crossover": 0.8,
        "mutation": 0.1,
        "GG": 0.2,
        "CF": 3,
        "skip": 0,
        "strategy":1

    }
    ,
    {
        "iterations": 4000,
        "size": 200,
        "crossover": 0.8,
        "mutation": 0.1,
        "GG": 0.3,
        "CF": 4,
        "skip": 0,
        "strategy":1

    },
    {
        "iterations": 4000,
        "size": 250,
        "crossover": 0.7,
        "mutation": 0.2,
        "GG": 0.2,
        "CF": 2,
        "skip": 0,
        "strategy":1

    }
];

exports.configurations=configurations;
generateStats();


function generateStats() {

    console.time('statsGeneration');

    var cyclesCount=10;
    console.stats=[];
    for(var z=0;z<cyclesCount;z++){
        console.log(`Прогін: ${z}`);
        console.stats.push({});

        testFunctions.testFunctions.forEach(functionConfig=> {
            console.log(functionConfig.func.name);

            console.stats[z][functionConfig.func.name]={};
            for (var n = 1; n <=5; ++n) {
                //console.log(`Розмірність: ${n}`)
                for (let i = 0; i < configurations.length; ++i) {
                    configurations[i].initialPopulation=testFunctions.generateInitialPopulation( configurations[i].size,n,functionConfig.min,functionConfig.max);
                }

                console.stats[z][functionConfig.func.name]['n'+n]=[];
                global.curAr=console.stats[z][functionConfig.func.name]['n'+n];
                    for (let i = 0; i < configurations.length; ++i) {
                    configurations[i].dimension=n;
                    //console.log(configurations[i].initialPopulation.length);
                    solver.executeWithStats(configurations[i], functionConfig,((z)=>{
                        console.z=z;
                        console.configurationNumber=i;

                        return function(stats){
                           var peeksInfo=this.utils.getMaxCount(stats.population,this.config);
                            //console.log(this);
                            //console.log(`Прогін: ${console.z}`);
                            //console.log(`Конфігурація: ${console.configurationNumber}`);

                            var data={
                                NFE:stats.generation_number*this.size,
                                number_of_peaks:peeksInfo.number_of_peaks,
                                real_peaks:peeksInfo.real_peaks,
                                effective_number:peeksInfo.number_of_peaks/this.config.peakCount(this.dimension)
                            };

                            if(this.config.peaks && this.dimension==1){
                                var mostSimilar={};
                                for(var i=0;i<stats.population.length;++i)
                                {

                                    for(var k=0;k<this.config.peaks.length;k++)
                                    {
                                        if(mostSimilar[k]==null){
                                            mostSimilar[k]=i;
                                        }
                                        else if(Math.abs(stats.population[mostSimilar[k]].x[0]-this.config.peaks[k])<Math.abs(stats.population[i].x[0]-this.config.peaks[k]))
                                        {
                                            mostSimilar[k]=i;
                                        }
                                    }
                                }
                                data.peak_accuracy=this.config.peaks.reduce((prev,xi,index)=>{
                                    prev+=Math.abs(this.config.func(xi)-stats.population[mostSimilar[index]].fitness);
                                    return prev;
                                },0);
                                function dist(x1,y1,x2,y2){
                                    return Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2))
                                }
                                data.distance_accuracy=this.config.peaks.reduce((prev,xi,index)=>{
                                    prev+=dist(xi,this.config.func(xi),stats.population[mostSimilar[index]].fitness,stats.population[mostSimilar[index]].x[0]);
                                   // prev+=Math.abs(this.config.func(xi)-stats.population[mostSimilar[index]].fitness);
                                    return prev;
                                },0);
                                //console.log(mostSimilar);
                            }
                            else
                            {

                            }
                            //console.log(data);
                            global.curAr.push(data);

                            //console.log(data);

                        };
                    })(z))

                }
            }


        });
    }




    //console.log(console.stats);
    fs.writeFile('data.json',JSON.stringify(console.stats,null, 4),function(er){

    });
    console.timeEnd('statsGeneration');



}

//console.log(dist(0,0,0,1));

